from django.conf.urls import patterns, include, url
from django.contrib import admin

from oscar.app import application

from django.conf import settings
from django.conf.urls.static import static

from Custom_apps.cart.app import application1

urlpatterns = [
	url(r'^i18n/', include('django.conf.urls.i18n')),
	# url(r'^register/', views.UserRegistration.as_view(), name='UserRegistration'),
	# The Django admin is not officially supported; expect breakage.
	# Nonetheless, it's often useful for debugging.
	url(r'^admin/', include(admin.site.urls)),
	url(r'', include(application.urls)),

	## Custom urls in cart
	url(r'', include(application1.urls)),

	


	# ----------------Custom Urls-----------------------------------
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
