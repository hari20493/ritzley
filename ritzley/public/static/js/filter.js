// Filtering function 
function filtering(category){

	$("#ajax").html("");
	$.ajax({
	url: '/filter/',
	data: {cate:category},
	dataType: 'json',
	type: "GET",
	contentType: "application/json;charset=utf-8",

	success: function (returnData) {
		content = eval(returnData);

		if (content.length > 1){

			var message_header = "<div class='col-md-12 sep-bottom-md'>\
            <h4>"+ content.length + " products were found for "+ category +"</h4></div>"
		}
		else{

			var message_header = "<div class='col-md-12 sep-bottom-md'>\
            <h4>"+ content.length + " product were found for "+ category +"</h4></div>"
		}

        $("#ajax").html(message_header);

		var html_content;
		for (var i = 0; i < content.length; i++) {

			if (content[i].discount != 0){

				html_content = "<div class='col-md-3 col-sm-6 col-xs-6 sep-bottom-lg'>\
				<a href="+content[i].slug+"_"+content[i].id+" class='product-image outline-outward'>\
				<img src="+ content[i].image +" alt="+ content[i].name +" class='img-responsive'>\
	            </a>\
		        <div class='product-title text-center'><span class='upper'>"+ content[i].category +"</span>\
	            <p>"+ content[i].name +"</p>\
	            <h6 class='stich-status'>"+ content[i].stitching +"</h6>\
	            <h6 class='stich-status'>"+ content[i].color +"</h6>\
	          	</div>\
	          	<div class='product-detail'>\
	            <div class='price-shop text-center'>\
	          	<del>&#x20B9;&nbsp;"+ content[i].price +"</del><ins>&#x20B9;&nbsp;"+ content[i].discount_amount +"</ins>\
	            </div>\
	          	</div>\
	            </div>";
			}
			else{

				html_content = "<div class='col-md-3 col-sm-6 col-xs-6 sep-bottom-lg'>\
				<a href="+content[i].slug+"_"+content[i].id+" class='product-image outline-outward'>\
				<img src="+ content[i].image +" alt="+ content[i].name +" class='img-responsive'>\
	            </a>\
		        <div class='product-title text-center'><span class='upper'>"+ content[i].category +"</span>\
	            <p>"+ content[i].name +"</p>\
	            <h6 class='stich-status'>"+ content[i].stitching +"</h6>\
	            <h6 class='stich-status'>"+ content[i].color +"</h6>\
	          	</div>\
	          	<div class='product-detail'>\
	            <div class='price-shop text-center'>\
	          	<ins>&#x20B9;&nbsp;"+ content[i].price +"</ins>\
	            </div>\
	          	</div>\
	            </div>";

			}


		$("#ajax").append(html_content);
		};

		
	},
	error: function(e) {

		var content = "<div class='col-md-12 sep-bottom-md'>\
                <h4>"+ e.responseText +"</h4></div>"
		$("#ajax").html(content);

  }

});


}

// Search Function
function searching(){

	$("#ajax").html("");
	keyword = $("#search").val();
	$.ajax({
	url: '/searchp/',
	data: {tag:keyword},
	dataType: 'json',
	type: "GET",
	contentType: "application/json;charset=utf-8",

	success: function (returnData) {
		content = eval(returnData);

		if (content.length > 1){

			var message_header = "<div class='col-md-12 sep-bottom-md'>\
            <h4>"+ content.length + " products were found for "+ keyword +"</h4></div>"
		}
		else{

			var message_header = "<div class='col-md-12 sep-bottom-md'>\
            <h4>"+ content.length + " product were found for "+ keyword +"</h4></div>"
		}

        $("#ajax").html(message_header);

		var html_content;

		for (var i = 0; i < content.length; i++) {

			if (content[i].discount != 0){

				html_content = "<div class='col-md-3 col-sm-6 col-xs-6 sep-bottom-lg'>\
				<a href="+content[i].slug+"_"+content[i].id+" class='product-image outline-outward'>\
				<img src="+ content[i].image +" alt="+ content[i].name +" class='img-responsive'>\
	            </a>\
		        <div class='product-title text-center'><span class='upper'>"+ content[i].category +"</span>\
	            <p>"+ content[i].name +"</p>\
	            <h6 class='stich-status'>"+ content[i].stitching +"</h6>\
	            <h6 class='stich-status'>"+ content[i].color +"</h6>\
	          	</div>\
	          	<div class='product-detail'>\
	            <div class='price-shop text-center'>\
	          	<del>&#x20B9;&nbsp;"+ content[i].price +"</del><ins>&#x20B9;&nbsp;"+ content[i].discount_amount +"</ins>\
	            </div>\
	          	</div>\
	            </div>";
			}
			else{

				html_content = "<div class='col-md-3 col-sm-6 col-xs-6 sep-bottom-lg'>\
				<a href="+content[i].slug+"_"+content[i].id+" class='product-image outline-outward'>\
				<img src="+ content[i].image +" alt="+ content[i].name +" class='img-responsive'>\
	            </a>\
		        <div class='product-title text-center'><span class='upper'>"+ content[i].category +"</span>\
	            <p>"+ content[i].name +"</p>\
	            <h6 class='stich-status'>"+ content[i].stitching +"</h6>\
	            <h6 class='stich-status'>"+ content[i].color +"</h6>\
	          	</div>\
	          	<div class='product-detail'>\
	            <div class='price-shop text-center'>\
	          	<ins>&#x20B9;&nbsp;"+ content[i].price +"</ins>\
	            </div>\
	          	</div>\
	            </div>";

			}


		$("#ajax").append(html_content);
		};


		
	},
	error: function(e) {

		var content = "<div class='col-md-12 sep-bottom-md'>\
                <h4>"+ e.responseText +"</h4></div>"
		$("#ajax").html(content);

  }

});


}


// Add boutique to favourite
function addtofav(){

	$("#bfav").attr('href', '#');
	$.ajax({
	url: '/addtofav/',
	type: "GET",

	success: function (returnData) {

		$("#bfav").attr('href', 'javascript:deltofav()');
		$("#bfav").css('color', '#CF1E75');
		$("#bfav").attr('data-tooltip', 'Remove from Favourite');
		$("#upperfav").hide();
		var data = "Added to Favourites"
		toastr.success(data, 'Ritzley');

	},
	error: function(e) {


  }

});


}


// Delete boutique to favourite
function deltofav(){

	$("#bfav").attr('href', '#');
	$.ajax({
	url: '/deltofav/',
	type: "GET",

	success: function (returnData) {

		$("#bfav").attr('href', 'javascript:addtofav()');
		$("#bfav").css('color', '#ddd');
		$("#bfav").attr('data-tooltip', 'Add to Favourite');
		var data = "Removed from Favourites"
		toastr.success(data, 'Ritzley');

	},
	error: function(e) {


  }

});


}


// Price Filter using price slider
$(document).ready(function() {

	// Declared here global variable property
	var price_range = "0-1300";
	$(".slider-selection").css({
		left: '0%',
		width: '12.6%'
	});
	$(".max-slider-handle").css('left', '13.4%');
	$(".min-slider-handle").css('left', '0%');

	$(".tooltip tooltip-main top hide").css({
		left: '6.7%',
		marginLeft: '0px'
	});

	$(".tooltip tooltip-max hide top").css({
		top: '-30px',
		left: '13.4%',
		marginLeft: '0px'
	});


	$(".priceslider").change(function(event) {
		
		price_range = $(this).val();
		price_range = price_range.replace(",", "-");

		$(".slidervalue").html(price_range)
	});

		// Ajax call starts
	$(".sliderbutton").click(function(event) {
		
		$("#ajax").html("");
		$.ajax({
		url: '/pricefilter/',
		data: {price:price_range},
		dataType: 'json',
		type: "GET",
		contentType: "application/json;charset=utf-8",

		success: function (returnData) {
			content = eval(returnData);

			if (content.length > 1){

			var message_header = "<div class='col-md-12 sep-bottom-md'>\
            <h4>"+ content.length + " products were found for price range of "+ price_range +"</h4></div>"
			}

			else{

			var message_header = "<div class='col-md-12 sep-bottom-md'>\
            <h4>"+ content.length + " product were found for price range of "+ price_range +"</h4></div>"
			}

			$("#ajax").html(message_header);

			var html_content;

			for (var i = 0; i < content.length; i++) {

				if (content[i].discount != 0){

					html_content = "<div class='col-md-3 col-sm-6 col-xs-6 sep-bottom-lg'>\
					<a href="+content[i].slug+"_"+content[i].id+" class='product-image outline-outward'>\
					<img src="+ content[i].image +" alt="+ content[i].name +" class='img-responsive'>\
		            </a>\
			        <div class='product-title text-center'><span class='upper'>"+ content[i].category +"</span>\
		            <p>"+ content[i].name +"</p>\
		            <h6 class='stich-status'>"+ content[i].stitching +"</h6>\
		            <h6 class='stich-status'>"+ content[i].color +"</h6>\
		          	</div>\
		          	<div class='product-detail'>\
		            <div class='price-shop text-center'>\
		          	<del>&#x20B9;&nbsp;"+ content[i].price +"</del><ins>&#x20B9;&nbsp;"+ content[i].discount_amount +"</ins>\
		            </div>\
		          	</div>\
		            </div>";
				}
				else{

					html_content = "<div class='col-md-3 col-sm-6 col-xs-6 sep-bottom-lg'>\
					<a href="+content[i].slug+"_"+content[i].id+" class='product-image outline-outward'>\
					<img src="+ content[i].image +" alt="+ content[i].name +" class='img-responsive'>\
		            </a>\
			        <div class='product-title text-center'><span class='upper'>"+ content[i].category +"</span>\
		            <p>"+ content[i].name +"</p>\
		            <h6 class='stich-status'>"+ content[i].stitching +"</h6>\
		            <h6 class='stich-status'>"+ content[i].color +"</h6>\
		          	</div>\
		          	<div class='product-detail'>\
		            <div class='price-shop text-center'>\
		          	<ins>&#x20B9;&nbsp;"+ content[i].price +"</ins>\
		            </div>\
		          	</div>\
		            </div>";

				}


			$("#ajax").append(html_content);
			};


			
		},
		error: function(e) {

			var content = "<div class='col-md-12 sep-bottom-md'>\
	                <h4>"+ e.responseText +"</h4></div>"
			$("#ajax").html(content);

	  }

	});
	// Ajax call ends Here
	});

});