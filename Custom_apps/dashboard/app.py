from oscar.apps.dashboard.app import DashboardApplication as CoreDashboardApplication
from django.conf.urls import url, include
from oscar.core.loading import get_class


class DashboardApplication(CoreDashboardApplication):

    permissions_map = {
        # 'index': (['is_staff'], ['partner.dashboard_access']),
    }
    
    DashboardLoginView = get_class('dashboard.views', 'DashboardLoginView')
    RitzleySettingsView = get_class('dashboard.views', 'RitzleySettingsView')
    StockAlert = get_class('dashboard.views', 'StockAlert')
    ChangeOrderStatusView = get_class('dashboard.views', 'ChangeOrderStatusView')
    # ReportView = get_class('dashboard.views', 'ReportView')
    boutique_app = get_class('dashboard.boutique.app', 'application')
    tailor_app = get_class('dashboard.tailor.app', 'application')

    def get_urls(self):
        urls = [
            url(r'^login/$', self.DashboardLoginView.as_view(), name='DashboardLoginView'),
            url(r'^settings/$', self.RitzleySettingsView.as_view(), name='ritzley-settings'),
            url(r'^stock/$', self.StockAlert.as_view(), name='stock-alert'),
            url(r'^change_order_status/$', self.ChangeOrderStatusView.as_view(), name='change-order-status'),
            # url(r'^report/$', self.ReportView.as_view(), name='report-list'),
            url(r'^$', self.index_view.as_view(), name='index'),
            url(r'^boutique/', include(self.boutique_app.urls)),
            url(r'^tailor/', include(self.tailor_app.urls)),
            url(r'^catalogue/', include(self.catalogue_app.urls)),
            url(r'^reports/', include(self.reports_app.urls)),
            url(r'^orders/', include(self.orders_app.urls)),
            url(r'^users/', include(self.users_app.urls)),
            url(r'^content-blocks/', include(self.promotions_app.urls)),
            url(r'^pages/', include(self.pages_app.urls)),
            url(r'^partners/', include(self.partners_app.urls)),
            url(r'^offers/', include(self.offers_app.urls)),
            url(r'^ranges/', include(self.ranges_app.urls)),
            url(r'^reviews/', include(self.reviews_app.urls)),
            url(r'^vouchers/', include(self.vouchers_app.urls)),
            url(r'^comms/', include(self.comms_app.urls)),
            url(r'^shipping/', include(self.shipping_app.urls)),
        ]
        return self.post_process_urls(urls)


application = DashboardApplication()


