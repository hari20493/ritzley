from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Tailor)
admin.site.register(TailorPortfolio)
admin.site.register(TailorCategory)
admin.site.register(TailorSubCategory)
admin.site.register(TailorPatterns)





