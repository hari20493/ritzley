from django.shortcuts import render, HttpResponse, render_to_response, redirect
from django.views.generic import View, TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.core.urlresolvers import reverse_lazy

from django.core.mail import EmailMessage
from django.core.exceptions import ObjectDoesNotExist

from .models import *
from Custom_apps.dashboard.models import *
from Custom_apps.dashboard.forms import *
from .forms import *





def SaveImage(request):

	image = request.FILES.get('file', 'Unknown.jpg')
	form = Product_image_form(request.POST, request.FILES)
	im = Product_image1(image=image)
	im.save()

	return im

def uploadCategoryImages(request):
	c={}

	if request.method == 'POST':
		im = SaveImage(request)
		request.session['cat_img'] = im.id
	else:
		return HttpResponse('Not OK')

	return HttpResponse('OK')

def uploadPortfolioImages(request):
	c={}

	if request.method == 'POST':
		im = SaveImage(request)
		request.session['port_img'] = im.id
	else:
		return HttpResponse('Not OK')

	return HttpResponse('OK')

def uploadPatternImages(request):
	c={}

	if request.method == 'POST':
		im = SaveImage(request)
		request.session['pat_img'] = im.id
	else:
		return HttpResponse('Not OK')

	return HttpResponse('OK')


# Ajax
class AddPortfolioAjaxView(View):

	def get(self, request, **kwargs):

		try:
			name = kwargs['name']
			tid = kwargs['tid']
			if name and tid:
				tid = int(tid)
				tailor = Tailor.objects.get(id=tid)
				if 'port_img' in request.session:
					img = request.session['port_img']
					img = int(img)
					image = Product_image1.objects.get(id=img)
					obj = TailorPortfolio(tailor=tailor, name=name, banner_image=image.image)
					obj.save()
					del request.session['port_img']
					out = obj.id
				else:
					obj = TailorPortfolio(tailor=tailor, name=name)
					obj.save()
					out = obj.id

			else:
				out = "0"
		except ObjectDoesNotExist, e:
			print str(e)
			out = "0"
		except AttributeError, e:
			print str(e)
			out = "0"

		return HttpResponse(out)

# Ajax
class AddCategoryAjaxView(View):

	def get(self, request, **kwargs):

		try:
			name = kwargs['name']
			base_price = kwargs['base_price']
			tid = kwargs['tid']
			if name and tid and base_price:
				tid = int(tid)
				tailor = Tailor.objects.get(id=tid)
				if 'cat_img' in request.session:
					img = request.session['cat_img']
					img = int(img)
					image = Product_image1.objects.get(id=img)
					obj =  TailorCategory(tailor=tailor, name=name, image=image.image, base_price=base_price)
					obj.save()
					del request.session['cat_img']
					out = obj.id
				else:
					obj = TailorCategory(tailor=tailor, name=name, base_price=base_price)
					obj.save()
					out = obj.id

			else:
				out = "0"
		except ObjectDoesNotExist, e:
			print str(e)
			out = "0"
		except AttributeError, e:
			print str(e)
			out = "0"

		return HttpResponse(out)


# Ajax
class AddSubCategoryView(View):

	def get(self, request, **kwargs):
		try:
			name = kwargs['name']
			category_id = kwargs['category_id']
			tid = kwargs['tid']

			if name and category_id and tid:
				tid = int(tid)
				tailor = Tailor.objects.get(id=tid)
				category = TailorCategory.objects.get(id=category_id)

				scats = TailorSubCategory.objects.filter(tailor=tailor, category=category)

				# limit maximum number of sub cats to 6
				if len(scats)>6:
					out = '-1'
				else:
					obj = TailorSubCategory(tailor=tailor, name=name, category=category)
					obj.save()
					out = obj.id

			else:
				out = "0"
		except ObjectDoesNotExist, e:
			print str(e)
			out = "0"
		except AttributeError, e:
			print str(e)
			out = "0"

		return HttpResponse(out)



# Ajax
class AddPatternAjaxView(View):

	def get(self, request, **kwargs):
		try:
			name = kwargs['name']
			price = kwargs['price']
			subcategory_id = kwargs['subcategory_id']
			tid = kwargs['tid']

			if name and price and subcategory_id and tid:
				tid = int(tid)
				subcategory_id = int(subcategory_id)
				tailor = Tailor.objects.get(id=tid)
				sub_category = TailorSubCategory.objects.get(id=subcategory_id)
				
				if 'pat_img' in request.session:
					img = request.session['pat_img']
					img = int(img)
					image = Product_image1.objects.get(id=img)

					obj =  TailorPatterns(tailor=tailor, name=name, image=image.image, price=price, sub_category=sub_category)
					obj.save()
					del request.session['pat_img']
					out = obj.id
				else:
					obj = TailorPatterns(tailor=tailor, name=name, price=price, sub_category=sub_category)
					obj.save()
					out = obj.id

			else:
				out = "0"
		except ObjectDoesNotExist, e:
			print str(e)
			out = "0"
		except AttributeError, e:
			print str(e)
			out = "0"

		return HttpResponse(out)






# View for listing tailors
class ListTailorView(ListView):
	"""docstring for ListTailorView"""
	
	model = Tailor
	context_object_name = "tailors"
	template_name = "dashboard/tailor/tailor_index.html"

	def get_context_data(self, **kwargs):

		context = super(ListTailorView, self).get_context_data(**kwargs)
		tailor_obj = Tailor.objects.all()
		if len(tailor_obj) == 0:
			context['no_data'] = "No tailor added"
		return context


# View for listing Portfolio
class ListPortfolioView(ListView):
	"""docstring for ListPortfolioView"""
	
	model = TailorPortfolio
	context_object_name = "portfolios"
	template_name = "dashboard/tailor/portfolio_index.html"

	def get_context_data(self, **kwargs):

		context = super(ListPortfolioView, self).get_context_data(**kwargs)
		port_obj = TailorPortfolio.objects.all()
		if len(port_obj) == 0:
			context['no_data'] = "No portfolio added"
		return context


# View for listing Category
class ListCategoryView(ListView):
	"""docstring for ListCategoryView"""
	
	model = TailorCategory
	context_object_name = "categories"
	template_name = "dashboard/tailor/category_index.html"

	def get_context_data(self, **kwargs):

		context = super(ListCategoryView, self).get_context_data(**kwargs)
		cate_obj = TailorCategory.objects.all()
		if len(cate_obj) == 0:
			context['no_data'] = "No service category added"
		return context


# View for listing Pattern
class ListPatternView(ListView):
	"""docstring for ListPatternView"""
	
	model = TailorPatterns
	context_object_name = "patterns"
	template_name = "dashboard/tailor/pattern_index.html"

	def get_context_data(self, **kwargs):

		context = super(ListPatternView, self).get_context_data(**kwargs)
		cate_obj = TailorPatterns.objects.all()
		if len(cate_obj) == 0:
			context['no_data'] = "No patterns added"
		return context


# View for adding Tailors
class AddTailorView(CreateView):
	"""docstring for AddTailorView"""
	
	model = Tailor
	fields = ['name', 'description', 'banner_image', 'logo', 'location', 'url', 'active', ]
	template_name = "dashboard/tailor/tailor_add.html"
	success_url = reverse_lazy('dashboard:tailor-list')


# View for adding Portfolio
class AddPortfolioView(CreateView):
	"""docstring for AddPortfolioView"""
	
	model = TailorPortfolio
	template_name = "dashboard/tailor/portfolio_add.html"
	success_url = reverse_lazy('dashboard:tailor-portfolio-list')


# View for adding Category
class AddCategoryView(CreateView):
	"""docstring for AddCategoryView"""
	
	model = TailorCategory
	template_name = "dashboard/tailor/category_add.html"
	success_url = reverse_lazy('dashboard:tailor-category-list')


# View for adding Pattern
class AddPatternView(CreateView):
	"""docstring for AddPatternView"""
	
	model = TailorPatterns
	template_name = "dashboard/tailor/pattern_add.html"
	success_url = reverse_lazy('dashboard:tailor-pattern-list')


# View for editing Tailors
class EditTailorView(UpdateView):
	"""docstring for EditTailorView"""
	
	model = Tailor
	fields = ['name', 'description', 'banner_image', 'logo', 'location', 'url', 'active', ]
	template_name = "dashboard/tailor/tailor_edit.html"
	success_url = reverse_lazy('dashboard:tailor-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		self.success_url = '/dashboard/tailor/'+tailor_id+'/'
		return self.success_url


# View for editing Portfolio
class EditPortfolioView(UpdateView):
	"""docstring for EditPortfolioView"""
	
	model = TailorPortfolio
	fields = ['name', 'banner_image',  ]
	template_name = "dashboard/tailor/portfolio_edit.html"
	success_url = reverse_lazy('dashboard:tailor-portfolio-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		self.success_url = '/dashboard/tailor/'+tailor_id+'/'
		return self.success_url



# View for editing Category
class EditCategoryView(UpdateView):
	"""docstring for EditCategoryView"""
	
	model = TailorCategory
	fields = ['name', 'image', 'base_price']
	template_name = "dashboard/tailor/category_edit.html"
	success_url = reverse_lazy('dashboard:tailor-category-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		self.success_url = '/dashboard/tailor/'+tailor_id+'/'
		return self.success_url



# View for editing Category
class EditSubCategoryView(UpdateView):
	"""docstring for EditCategoryView"""
	
	model = TailorSubCategory
	fields = ['name', ]
	template_name = "dashboard/tailor/subcategory_edit.html"
	success_url = reverse_lazy('dashboard:tailor-category-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		category_id = self.request.GET.get('category_id', '')

		self.success_url = '/dashboard/tailor/'+tailor_id+'/'+category_id+'/'
		return self.success_url


# View for editing Pattern
class EditPatternView(UpdateView):
	"""docstring for EditPatternView"""
	
	model = TailorPatterns
	fields = ['sub_category', 'name', 'price', 'image', ]
	template_name = "dashboard/tailor/pattern_edit.html"
	success_url = reverse_lazy('dashboard:tailor-pattern-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		category_id = self.request.GET.get('category_id', '')

		self.success_url = '/dashboard/tailor/'+tailor_id+'/'+category_id+'/'
		return self.success_url


# This view delete the Tailor
class DeleteTailorView(DeleteView):
	
	model = Tailor
	context_object_name = "tailor"
	template_name = "dashboard/tailor/tailor_delete.html"
	success_url = reverse_lazy('dashboard:tailor-list')


# This view delete the Portfolio
class DeletePortfolioView(DeleteView):
	
	model = TailorPortfolio
	context_object_name = "portfolio"
	template_name = "dashboard/tailor/portfolio_delete.html"
	success_url = reverse_lazy('dashboard:tailor-portfolio-list')
	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		self.success_url = '/dashboard/tailor/'+tailor_id+'/'
		return self.success_url


# This view delete the Category
class DeleteCategoryView(DeleteView):
	
	model = TailorCategory
	context_object_name = "category"
	template_name = "dashboard/tailor/category_delete.html"
	success_url = reverse_lazy('dashboard:tailor-category-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		self.success_url = '/dashboard/tailor/'+tailor_id+'/'
		return self.success_url


# This view delete the Category
class DeleteSubCategoryView(DeleteView):
	
	model = TailorSubCategory
	context_object_name = "subcategory"
	template_name = "dashboard/tailor/subcategory_delete.html"
	success_url = reverse_lazy('dashboard:tailor-category-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		category_id = self.request.GET.get('category_id', '')

		self.success_url = '/dashboard/tailor/'+tailor_id+'/'+category_id+'/'
		return self.success_url


# This view delete the Tailor
class DeletePatternView(DeleteView):
	
	model = TailorPatterns
	context_object_name = "pattern"
	template_name = "dashboard/tailor/pattern_delete.html"
	success_url = reverse_lazy('dashboard:tailor-pattern-list')

	def get_success_url(self):
		tailor_id = self.request.GET.get('tailor_id', '')
		category_id = self.request.GET.get('category_id', '')

		self.success_url = '/dashboard/tailor/'+tailor_id+'/'+category_id+'/'
		return self.success_url




class DetailTailorView(TemplateView):
	
	model = Tailor
	# context_object_name = "object"
	template_name = "dashboard/tailor/tailor_add_params.html"
	success_url = reverse_lazy('dashboard:tailor-list')



	def get_context_data(self, **kwargs):
		context = super(DetailTailorView, self).get_context_data(**kwargs)

		tid = kwargs['pk']
		tailor = Tailor.objects.get(id=tid)

		context['form_portfolio'] = TailorPortfolioForm(prefix="form_port")
		context['form_category'] = TailorCategoryForm(prefix="form_cat")

		context['object'] = tailor

		context['services'] = TailorCategory.objects.filter(tailor=tailor)
		context['portfolios'] = TailorPortfolio.objects.filter(tailor=tailor)

		return context



class DetailTailorSubcatView(TemplateView):

	template_name = "dashboard/tailor/tailor_add_subcats.html"
	success_url = reverse_lazy('dashboard:tailor-list')


	def get_context_data(self, **kwargs):
		context = super(DetailTailorSubcatView, self).get_context_data(**kwargs)

		tailor_id = kwargs['tailor_id']
		cat_id = kwargs['category_id']

		tailor = Tailor.objects.get(id=tailor_id)
		category = TailorCategory.objects.get(id=cat_id)

		context['tailor'] = tailor
		context['object'] = category

		context['form_sub_category'] = TailorSubCategoryForm(prefix="form_subcat")
		context['form_pattern'] = TailorPatternsForm(prefix="form_pat")

		subs = TailorSubCategory.objects.filter(tailor=tailor, category=category)

		context['sub_category'] = subs
		context['patterns'] = TailorPatterns.objects.filter(tailor=tailor, sub_category=subs)

		return context










