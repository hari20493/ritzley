from django.conf.urls import url

from oscar.core.loading import get_class
from oscar.core.application import Application
from .views import *


class TailorDashboardApplication(Application):
    name = None
    default_permissions = ['is_staff', ]

    list_view = get_class('dashboard.tailor.views', 'ListTailorView')
    list_portfolio_view = get_class('dashboard.tailor.views', 'ListPortfolioView')
    list_category_view = get_class('dashboard.tailor.views', 'ListCategoryView')
    list_pattern_view = get_class('dashboard.tailor.views', 'ListPatternView')
    add_view = get_class('dashboard.tailor.views', 'AddTailorView')
    add_portfolio_view = get_class('dashboard.tailor.views', 'AddPortfolioView')
    add_category_view = get_class('dashboard.tailor.views', 'AddCategoryView')
    add_pattern_view = get_class('dashboard.tailor.views', 'AddPatternView')

    edit_view = get_class('dashboard.tailor.views', 'EditTailorView')
    edit_portfolio_view = get_class('dashboard.tailor.views', 'EditPortfolioView')
    edit_category_view = get_class('dashboard.tailor.views', 'EditCategoryView')
    edit_subcategory_view = get_class('dashboard.tailor.views', 'EditSubCategoryView')
    edit_pattern_view = get_class('dashboard.tailor.views', 'EditPatternView')

    delete_tailor_view = get_class('dashboard.tailor.views', 'DeleteTailorView')
    delete_portfolio_view = get_class('dashboard.tailor.views', 'DeletePortfolioView')
    delete_category_view = get_class('dashboard.tailor.views', 'DeleteCategoryView')
    delete_subcategory_view = get_class('dashboard.tailor.views', 'DeleteSubCategoryView')
    delete_pattern_view = get_class('dashboard.tailor.views', 'DeletePatternView')

    detail_tailor_view = get_class('dashboard.tailor.views', 'DetailTailorView')
    detail_subcat_tailor_view = get_class('dashboard.tailor.views', 'DetailTailorSubcatView')


    #ajax
    add_portfolio_ajax_view = get_class('dashboard.tailor.views', 'AddPortfolioAjaxView')
    add_category_ajax_view = get_class('dashboard.tailor.views', 'AddCategoryAjaxView')
    add_pattern_ajax_view = get_class('dashboard.tailor.views', 'AddPatternAjaxView')
    add_subcategory_ajax_view = get_class('dashboard.tailor.views', 'AddSubCategoryView')



    def get_urls(self):
        urls = [
            url(r'^$', self.list_view.as_view(), name='tailor-list'),

            url(r'^(?P<pk>\d+)/$', self.detail_tailor_view.as_view(), name='tailor-details'),
            url(r'^(?P<tailor_id>\d+)/(?P<category_id>\d+)/$', self.detail_subcat_tailor_view.as_view(), name='tailor-subcats-details'),

            url(r'^portfolio/$', self.list_portfolio_view.as_view(), name='tailor-portfolio-list'),
            url(r'^category/$', self.list_category_view.as_view(), name='tailor-category-list'),
            url(r'^pattern/$', self.list_pattern_view.as_view(), name='tailor-pattern-list'),

            url(r'^add/$', self.add_view.as_view(), name='add-tailor'),
            url(r'^portfolio/add/$', self.add_portfolio_view.as_view(), name='add-tailor-portfolio'),
            url(r'^category/add/$', self.add_category_view.as_view(), name='add-tailor-category'),
            url(r'^pattern/add/$', self.add_pattern_view.as_view(), name='add-tailor-pattern'),

            url(r'^edit/(?P<pk>\d+)/$', self.edit_view.as_view(), name='edit-tailor'),
            url(r'^portfolio/edit/(?P<pk>\d+)/$', self.edit_portfolio_view.as_view(), name='edit-tailor-portfolio'),
            url(r'^category/edit/(?P<pk>\d+)/$', self.edit_category_view.as_view(), name='edit-tailor-category'),
            url(r'^subcategory/edit/(?P<pk>\d+)/$', self.edit_subcategory_view.as_view(), name='edit-tailor-category'),
            url(r'^pattern/edit/(?P<pk>\d+)/$', self.edit_pattern_view.as_view(), name='edit-tailor-pattern'),

            url(r'^delete/(?P<pk>\d+)/$', self.delete_tailor_view.as_view(), name='delete-tailor'),
            url(r'^portfolio/delete/(?P<pk>\d+)/$', self.delete_portfolio_view.as_view(), name='delete-portfolio'),
            url(r'^category/delete/(?P<pk>\d+)/$', self.delete_category_view.as_view(), name='delete-category'),
            url(r'^subcategory/delete/(?P<pk>\d+)/$', self.delete_subcategory_view.as_view(), name='delete-subcategory'),
            url(r'^pattern/delete/(?P<pk>\d+)/$', self.delete_pattern_view.as_view(), name='delete-pattern'),

            url(r'^portfolio/add/ajax/(?P<name>.*)/(?P<tid>\d+)/$', self.add_portfolio_ajax_view.as_view(), name='add-tailor-portfolio-ajax'),
            url(r'^category/add/ajax/(?P<name>.*)/(?P<base_price>\d+)/(?P<tid>\d+)/$', self.add_category_ajax_view.as_view(), name='add-tailor-category-ajax'),

            url(r'^pattern/add/ajax/(?P<price>\d+)/(?P<subcategory_id>\d+)/(?P<name>.*)/(?P<tid>\d+)/(?P<cat_id>\d+)/$', self.add_pattern_ajax_view.as_view(), name='add-tailor-pattern-ajax'),
            url(r'^subcategory/add/ajax/(?P<category_id>\d+)/(?P<name>.*)/(?P<tid>\d+)/$', self.add_subcategory_ajax_view.as_view(), name='add-tailor-subcategory-ajax'),

            url(r'^portfolio/add/image/$', uploadPortfolioImages, name='add-tailor-portfolio-image'),
            url(r'^pattern/add/image/$', uploadPatternImages, name='add-tailor-pattern-image'),
            url(r'^category/add/image/$', uploadCategoryImages, name='add-tailor-category-image'),


        ]
        return self.post_process_urls(urls)


application = TailorDashboardApplication()
