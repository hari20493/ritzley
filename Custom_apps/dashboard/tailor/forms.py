from django import forms
from models import *
from Custom_apps.dashboard.tailor.models import *


class TailorForm(forms.ModelForm):

    class Meta:
        model = Tailor
        exclude = [],


class TailorPortfolioForm(forms.ModelForm):

    class Meta:
        model = TailorPortfolio
        exclude = [],


class TailorCategoryForm(forms.ModelForm):

    class Meta:
        model = TailorCategory
        exclude = [],


class TailorSubCategoryForm(forms.ModelForm):

    class Meta:
        model = TailorSubCategory
        exclude = [],


class TailorPatternsForm(forms.ModelForm):

    class Meta:
        model = TailorPatterns
        exclude = [],
