# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0005_tailorcategory_base_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tailor',
            name='url',
            field=models.CharField(default=b'ritzley.com', max_length=1024),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tailorcategory',
            name='tailor',
            field=models.ForeignKey(default='', to='tailor.Tailor'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tailorpatterns',
            name='sub_category',
            field=models.ForeignKey(default='', to='tailor.TailorSubCategory'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tailorpatterns',
            name='tailor',
            field=models.ForeignKey(default='', to='tailor.Tailor'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tailorportfolio',
            name='tailor',
            field=models.ForeignKey(default='', to='tailor.Tailor'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tailorsubcategory',
            name='category',
            field=models.ForeignKey(default='', to='tailor.TailorCategory'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tailorsubcategory',
            name='tailor',
            field=models.ForeignKey(default='', to='tailor.Tailor'),
            preserve_default=False,
        ),
    ]
