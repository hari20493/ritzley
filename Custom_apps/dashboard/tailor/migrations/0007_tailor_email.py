# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0006_auto_20150713_1433'),
    ]

    operations = [
        migrations.AddField(
            model_name='tailor',
            name='email',
            field=models.CharField(max_length=1024, blank=True),
            preserve_default=True,
        ),
    ]
