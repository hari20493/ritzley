# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0004_tailorsubcategory_tailor'),
    ]

    operations = [
        migrations.AddField(
            model_name='tailorcategory',
            name='base_price',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
