# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tailorcategory',
            name='sub_category',
            field=models.ManyToManyField(to='tailor.TailorSubCategory'),
            preserve_default=True,
        ),
    ]
