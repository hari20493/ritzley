# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0003_auto_20150708_0544'),
    ]

    operations = [
        migrations.AddField(
            model_name='tailorsubcategory',
            name='tailor',
            field=models.ForeignKey(blank=True, to='tailor.Tailor', null=True),
            preserve_default=True,
        ),
    ]
