# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0002_tailorcategory_sub_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tailor',
            name='portfolio',
        ),
        migrations.RemoveField(
            model_name='tailor',
            name='service_offered',
        ),
        migrations.RemoveField(
            model_name='tailorcategory',
            name='sub_category',
        ),
        migrations.RemoveField(
            model_name='tailorsubcategory',
            name='patterns',
        ),
        migrations.AddField(
            model_name='tailorcategory',
            name='tailor',
            field=models.ForeignKey(blank=True, to='tailor.Tailor', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tailorpatterns',
            name='sub_category',
            field=models.ForeignKey(blank=True, to='tailor.TailorSubCategory', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tailorpatterns',
            name='tailor',
            field=models.ForeignKey(blank=True, to='tailor.Tailor', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tailorportfolio',
            name='tailor',
            field=models.ForeignKey(blank=True, to='tailor.Tailor', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tailorsubcategory',
            name='category',
            field=models.ForeignKey(blank=True, to='tailor.TailorCategory', null=True),
            preserve_default=True,
        ),
    ]
