# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tailor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(null=True, blank=True)),
                ('banner_image', models.ImageField(default=b'Unknown.jpg', null=True, upload_to=b'', blank=True)),
                ('logo', models.ImageField(default=b'Unknown.jpg', null=True, upload_to=b'', blank=True)),
                ('location', models.CharField(max_length=50, null=True, blank=True)),
                ('url', models.CharField(default=b'ritzley.com', max_length=1024, null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TailorCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(default=b'Unknown.jpg', upload_to=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TailorPatterns',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('image', models.ImageField(default=b'Unknown.jpg', upload_to=b'')),
                ('price', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TailorPortfolio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('banner_image', models.ImageField(default=b'Unknown.jpg', upload_to=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TailorSubCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('patterns', models.ManyToManyField(to='tailor.TailorPatterns')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='tailor',
            name='portfolio',
            field=models.ManyToManyField(to='tailor.TailorPortfolio', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tailor',
            name='service_offered',
            field=models.ManyToManyField(to='tailor.TailorCategory', null=True, blank=True),
            preserve_default=True,
        ),
    ]
