from django.db import models
# Create your models here.



# Tailor Model
class Tailor(models.Model):
	# TODO: Define fields here
	
	name = models.CharField(max_length=50, blank=False, null=False)
	description = models.TextField(blank=True, null=True)
	banner_image = models.ImageField(default="Unknown.jpg", blank=True, null=True)
	logo = models.ImageField(default="Unknown.jpg", blank=True, null=True)
	location = models.CharField(blank=True, null=True, max_length=50)
	url = models.CharField(max_length=1024, default="ritzley.com", null=False, blank=False)
	active = models.BooleanField(default=True)
	email = models.CharField(max_length = 1024,blank=True)
	# service_offered = models.ManyToManyField(TailorCategory, blank=True, null=True)
	# portfolio = models.ManyToManyField(TailorPortfolio, blank=True, null=True)
	
	def __unicode__(self):
		return "{0} - {1}".format(self.name, self.url)


class TailorPortfolio(models.Model):
	# TODO: Define fields here

	tailor = models.ForeignKey(Tailor, null=False, blank=False)
	name = models.CharField(max_length=50, blank=False, null=False)
	banner_image = models.ImageField(default="Unknown.jpg", blank=False, null=False)
	
	def __unicode__(self):
		return "{0} - {1}".format(self.name, self.tailor.name)


# Tailor Category Model
class TailorCategory(models.Model):
	# TODO: Define fields here
	
	tailor = models.ForeignKey(Tailor, null=False, blank=False)
	name = models.CharField(max_length=50, blank=False, null=False)
	image = models.ImageField(default="Unknown.jpg")
	base_price = models.IntegerField(default=0)
	# sub_category = models.ManyToManyField(TailorSubCategory)

	def __unicode__(self):
		return "{0} - {1}".format(self.name, self.tailor.name)


# Tailor Sub-Category Model
class TailorSubCategory(models.Model):
	# TODO: Define fields here
	tailor = models.ForeignKey(Tailor, null=False, blank=False)
	category = models.ForeignKey(TailorCategory, null=False, blank=False)
	name = models.CharField(max_length=50, blank=False, null=False)
	# patterns = models.ManyToManyField(TailorPatterns)

	def __unicode__(self):
		return "{0} - {1}".format(self.name, self.category.name)



# Tailor Patterns Model
class TailorPatterns(models.Model):
	# TODO: Define fields here

	tailor = models.ForeignKey(Tailor, null=False, blank=False)
	sub_category = models.ForeignKey(TailorSubCategory, null=False, blank=False)
	name = models.CharField(max_length=50, blank=False, null=False)
	image = models.ImageField(default="Unknown.jpg")
	price = models.IntegerField(default=0)

	def __unicode__(self):
		return "{0} - {1} - {2}".format(self.name, self.tailor.name, self.sub_category.name)

