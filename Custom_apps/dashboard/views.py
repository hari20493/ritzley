from django.views.generic import View, TemplateView, ListView, DetailView
from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from oscar.apps.dashboard.views import IndexView as CoreIndexView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from .models import *
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from Custom_apps.cart.models import Transaction
from django.template.loader import get_template


# from Custom_apps.cart.models import *


# ------------------------------Functions-----------------------------------------
def staff_required(login_url=None):
	return user_passes_test(lambda u: u.is_staff, login_url=login_url)


#---------------------------------Views------------------------------------------- 
# Write your views here

# Mailer function
 
def mailer(order_id,status):
	from_email = "support@ritzley.com"
	transaction_obj = Transaction.objects.get(id=order_id)
	data = {}
	if transaction_obj.status == "canceled":
		if transaction_obj.product:
			data['product_name'] = transaction_obj.product.name
			html = get_template("boutique_mailer_canceled.html")
			to = transaction_obj.product.boutique.email
		else:
			data['product_name'] = transaction_obj.tailor.name + " " + transaction_obj.tailor.category
			html = get_template("boutique_mailer_canceled.html")
			to = transaction_obj.tailor.email


	
	d = data
	html_content = html.render(d)

	try:
		email = EmailMessage(subject, html_content, from_email, [to])
		email.content_subtype = "html"  # Main content is now text/html
		email.send()
	except Exception, e:
		pass



"""
This is a custom login window view for dashboard panel
"""
class DashboardLoginView(TemplateView):
	"""docstring for DashboardLoginView"""
	
	template_name = 'dashboard/dashboard-login.html'

	""" Method for rendering the login page and redirect if user already logged in """
	def get(self, request):
		
		context = super(DashboardLoginView, self).get(request)
		if request.user.is_authenticated():

			return redirect(reverse_lazy('dashboard:index'))
		return context

	""" 
	Method of checking whether the enter username and password is redirect to corresponding pages
	and giving appropriate messages
	"""
	def post(self, request):

		username = request.POST.get('username','')
		password = request.POST.get('password','')

		user = authenticate(username=username, password=password)

		if user is not None:
			if user.is_active:

				if user.is_superuser:
					login(request, user)
					messages.success(request, "Welcome " + username)
				else:
					messages.warning(request, "You don't have permission to access this section")
			else:
				messages.warning(request, "Your account has been disabled")
		else:
			messages.error(request, "Your Username/Password is incorrect")
		
		return redirect(reverse_lazy('dashboard:index'))
		

"""
This is dashboard panel index view
"""
class IndexView(CoreIndexView):
	
	def get_template_names(self):

		# if not self.request.user.is_authenticated():
		# 	return ['dashboard/dashboard-login.html']
		if self.request.user.is_staff:
			return ['dashboard/index.html', ]
		else:
			return ['404.html',]

	@method_decorator(login_required(login_url='/dashboard/login/'))
	def dispatch(self, request, *args, **kwargs):
		return super(IndexView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)

		total_products = len(Product.objects.all())
		closed_low_stock = len(Product.objects.filter(size__stock=0))
		open_low_stock = len(Product.objects.filter(size__stock__lte=4))

		# Reports section
		date_from = datetime.datetime.now() - datetime.timedelta(days=1)
		result = Transaction.objects.filter(date_of_purchase__gte=date_from)

		context['orders'] = result
		context['statuses'] = Transaction.STATUS

		context['total_products'] = total_products
		context['total_open_stock_alerts'] = open_low_stock
		context['total_closed_stock_alerts'] = closed_low_stock

		return context

	def post(self, request, **kwargs):

		context = {}

		from_date = request.POST.get('from_date', '')
		to_date = request.POST.get('to_date', '')

		if from_date and to_date:
			result = Transaction.objects.filter(date_of_purchase__gte=from_date, date_of_purchase__lte=to_date)

			context['orders'] = result
			context['from_date'] = from_date
			context['to_date'] = to_date
		else:
			messages.error(request, "Error! Missing args!")

		context['statuses'] = Transaction.STATUS

		return render(request, self.template_name, context)


"""
This view is for rendering and adding Ritzley common settings
"""
class RitzleySettingsView(TemplateView):
	"""docstring for RitzleySettingsView"""
	
	template_name = "dashboard/settings.html"

	@method_decorator(staff_required(login_url='/dashboard/login/'))
	def dispatch(self, request, *args, **kwargs):
		return super(RitzleySettingsView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):

		context = super(RitzleySettingsView, self).get_context_data(**kwargs)
		try:
			Faq_obj = Faq.objects.latest('id')
			context['Faq'] = Faq_obj
		except ObjectDoesNotExist, e:
			pass

		return context

	def post(self, request):
		
		FAQ = request.POST.get("FAQ","")
		Data_type = request.POST.get("type","")

		if Data_type == "Add":
			Faq(description=FAQ).save()
		else:
			Faq_id = request.POST.get("id","")
			Faq_obj = Faq.objects.get(id=Faq_id)
			Faq_obj.description = FAQ
			Faq_obj.save()


		return redirect(reverse('dashboard:ritzley-settings'))
		

"""This view renders the low stock alert"""
class StockAlert(TemplateView):
	"""docstring for StockAlert"""
	
	template_name = "dashboard/stock_alerts.html"

	@method_decorator(staff_required(login_url='/dashboard/login/'))
	def dispatch(self, request, *args, **kwargs):
		return super(StockAlert, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):

		status = self.request.GET.get('status','')
		context = super(StockAlert, self).get_context_data(**kwargs)

		if status == "Open":
			prod_obj = Product.objects.filter(size__stock__lte=4).distinct()
		elif status == "Closed":
			prod_obj = Product.objects.filter(size__stock=0).distinct()
		else:
			prod_obj = Product.objects.filter( Q(size__stock__lte=4) | Q(size__stock=0) ).distinct()

		context['low_stock_list'] = prod_obj
		return context



# """This view renders the Report page"""
# class ReportView(TemplateView):
# 	"""docstring for ReportView"""
	
# 	template_name = "dashboard/report.html"

# 	@method_decorator(staff_required(login_url='/dashboard/login/'))
# 	def dispatch(self, request, *args, **kwargs):
# 	    return super(ReportView, self).dispatch(request, *args, **kwargs)

# 	def get_context_data(self, **kwargs):
# 		context = super(ReportView, self).get_context_data(**kwargs)

# 		date_from = datetime.datetime.now() - datetime.timedelta(days=1)
# 		result = Transaction.objects.filter(date_of_purchase__gte=date_from)

# 		context['orders'] = result
# 		context['statuses'] = Transaction.STATUS

# 		return context

# 	def post(self, request, **kwargs):

# 		context = {}

# 		from_date = request.POST.get('from_date', '')
# 		to_date = request.POST.get('to_date', '')

# 		if from_date and to_date:
# 			result = Transaction.objects.filter(date_of_purchase__gte=from_date, date_of_purchase__lte=to_date)

# 			context['orders'] = result
# 			context['from_date'] = from_date
# 			context['to_date'] = to_date
# 		else:
# 			messages.error(request, "Error! Missing args!")

# 		context['statuses'] = Transaction.STATUS

# 		return render(request, self.template_name, context)


# view to change ststus of a transaction

class ChangeOrderStatusView(TemplateView):

	template_name = "dashboard/report.html"

	def post(self, request, **kwargs):

		context = {}

		order_id = request.POST.get('order_id', '')
		status = request.POST.get('status', '')

		if order_id and status:
			result = Transaction.objects.get(id=order_id)
			result.status = status
			result.save()
			mailer(order_id,status)

			messages.success(request, "Status of selected transaction has been changed to "+str(status)) 
		else:
			messages.error(request, "Error! Missing args!")

		return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

	def get(self, request, **kwargs):

		return HttpResponseRedirect(reverse('dashboard:report-list'))


