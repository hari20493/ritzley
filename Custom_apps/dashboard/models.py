from django.db import models
# from oscar.apps.dashboard.models import *
from django.contrib.auth.models import User
from Custom_apps.dashboard.tailor.models import Tailor, TailorCategory

import datetime


class Faq(models.Model):

	description = models.TextField()

	class Meta:
		verbose_name = "Faq"
		verbose_name_plural = "Faqs"

	def __unicode__(self):
		return self.description



class Templatepage(models.Model):
	# TODO: Define fields here
	
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return self.name


class Color(models.Model):

	name = models.CharField(blank=False, null=False,max_length=50)

	def __unicode__(self):
		return self.name


"""
If more stiching category needed to be entered, then use below model for stiching
"""
# class Stiching(models.Model):

# 	title = models.CharField(blank=False, null=False,max_length=50)

# 	def __unicode__(self):
# 		return self.title


class Boutique(models.Model):
	# TODO: Define fields here
	
	name = models.CharField(max_length=50, blank=False, null=False)
	description = models.TextField(blank=True, null=True)
	faq = models.TextField(blank=True, null=True)
	refund_policy = models.TextField(blank=True, null=True)
	terms_conditions = models.TextField(blank=True, null=True)
	header_image = models.ImageField(default="Unknown.jpg",blank=True, null=True)
	logo = models.ImageField(default="Unknown.jpg", blank=True, null=True)
	placeholder_1 = models.ImageField(default="Unknown.jpg", blank=True, null=True, verbose_name='Placeholder 1')
	placeholder_2 = models.ImageField(default="Unknown.jpg", blank=True, null=True, verbose_name='Placeholder 2')
	location = models.CharField(blank=False, null=False, max_length=50)
	featured = models.BooleanField(default=False)
	template = models.ForeignKey(Templatepage, blank=False, null=False, default=False)
	url = models.CharField(max_length=1024, default="ritzley.com", null=False, blank=False)
	fb_url = models.CharField(max_length=1024, default="facebook.com", null=True, blank=True)
	tailor = models.ForeignKey(Tailor, blank=True, null=True)
	active = models.BooleanField(default=True)
	email = models.CharField(max_length = 1024,blank=True)
	
	def __unicode__(self):
		return self.name


# Category for products
class Category(models.Model):

	name = models.CharField(blank=False, null=False, max_length=50)
	def __unicode__(self):
		return self.name


# Product size for product. This is inserted in seperate table because each size has its on stock
class Product_size1(models.Model):
	
	product_size = models.CharField(blank=False, null=False, max_length=50)
	stock = models.IntegerField(blank=False, null=False, default=0)

	def __unicode__(self):
		return "{0} - {1} - {2}".format(self.id, self.product_size, self.stock)

# Images for products are used
class Product_image1(models.Model):

	image = models.ImageField(default="Unknown.jpg")

	def __unicode__(self):
		return self.image.name


# Product model for inserting products 
class Product(models.Model):


	STITCHED = 'Stitched'
	HALF_STITCHED = 'Half-Stitched'
	FULL_STITCHED = 'Non-Stitched'

	STITCHING = (
		(STITCHED, 'Stitched'),
		(HALF_STITCHED, 'Half-Stitched'),
		(FULL_STITCHED, 'Non-Stitched'),
	)

	boutique = models.ForeignKey(Boutique, blank=False, null=False, related_name="boutique", default=True)
	category = models.ForeignKey(Category, blank=False, null=False, default=True)
	name = models.CharField(blank=False, null=False, max_length=50,default="")
	# stock = models.IntegerField(default=0)
	price = models.IntegerField(blank=False, null=False, max_length=50,default=0)
	description = models.TextField(blank=True, null=True, max_length=1000)
	tags = models.CharField(blank=True, null=True, max_length=1000)
	discount = models.IntegerField(default = 0, blank=True, null=True)
	discount_amount = models.IntegerField(default = 0, blank=True, null=True)
	color = models.ForeignKey(Color, blank=False, null=False, default=True)
	# stiching = models.ForeignKey(Stiching, blank=False, null=False, default=True)
	stitching = models.CharField(max_length=50, choices=STITCHING, default=STITCHED)
	added_date = models.DateTimeField('date added', auto_now=True, auto_now_add=True, default=datetime.date.today())
	featured = models.BooleanField(default=False)

	image = models.ManyToManyField(Product_image1, null=True, blank=True)
	size = models.ManyToManyField(Product_size1, null=True, blank=True)

	def __unicode__(self):
		return "{0} - {1} - {2}".format(self.id, self.name, self.stitching)


"""Model for capturing user's favourite boutiques"""
class Boutique_favourite(models.Model):

	user = models.ForeignKey(User, blank=False, null=False, default=True)
	boutique = models.ForeignKey(Boutique, blank=False, null=False, default=True)

	def __unicode__(self):
		return self.boutique.name









	