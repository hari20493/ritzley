from django import forms
from models import *
from Custom_apps.dashboard.models import *

# Create your forms here

class BoutiqueForm(forms.ModelForm):

    class Meta:
        model = Boutique
        exclude = [],


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = ['category', 'name', 'price', 'description', 'tags', 'discount','featured', 'image', 'boutique', 'stitching', ]
        exclude = ['boutique', 'discount_amount', 'added_date,', 'size', 'image', ]


class Product_image_form(forms.ModelForm):

    class Meta:
        model = Product_image1
        exclude = [],



class Product_size_form(forms.ModelForm):

    class Meta:
        model = Product_size1
        exclude = [],


class DocumentForm(forms.Form):
    image = forms.ImageField()

