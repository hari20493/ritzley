from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import View, TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.core.urlresolvers import reverse_lazy, reverse
from Custom_apps.dashboard.models import Boutique
from django.contrib import messages

from django.forms.formsets import formset_factory, BaseFormSet
from django.forms.models import modelformset_factory

from Custom_apps.dashboard.forms import *
from Custom_apps.dashboard.models import *
from django.core.exceptions import ObjectDoesNotExist



# Create Your views here
# This displays the boutique list
class boutique_list_view(ListView):

	model = Boutique
	context_object_name = "boutiques"
	template_name = "dashboard/boutique/boutique_index.html"

	def get_context_data(self, **kwargs):
		context = super(boutique_list_view, self).get_context_data(**kwargs)
		cate_obj = Category.objects.all()
		context['categorys'] = cate_obj
		return context


# Add Boutiques
class add_boutique_view(TemplateView):

	Form  = BoutiqueForm()
	template_name = "dashboard/boutique/boutique_add.html"

	# renders from this method when entering the url
	def get_context_data(self, **kwargs):

		context = super(add_boutique_view, self).get_context_data(**kwargs)
		context['form'] = self.Form
		context['form_action'] = "/dashboard/boutique/add/"
		return context

	# On button click post method will accessed to save the details and image files into the database
	def post(self, request):

		context = {}
		form = BoutiqueForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			messages.success(request, 'Added new boutique')
		else:
			messages.error(request, 'Please fill all fields')
			return HttpResponseRedirect(reverse_lazy('dashboard:add-boutique'))

		context['form'] = self.Form
		return HttpResponseRedirect(reverse_lazy('dashboard:boutique-list'))


# Delete Boutiques
class delete_boutique_view(DeleteView):

	model = Boutique
	context_object_name = "boutique"
	template_name = "dashboard/boutique/boutique_delete.html"
	success_url = reverse_lazy('dashboard:boutique-list')


# Detailed view of Boutiques, This view also adds the product according to boutique
class boutique_detail_view(DetailView):

	model = Boutique
	context_object_name = "boutique"
	template_name = "dashboard/boutique/boutique_details.html"

	def get_context_data(self, **kwargs):

		context = super(boutique_detail_view, self).get_context_data(**kwargs)
		bout_obj = self.get_object()
		pro_obj = Product.objects.filter(boutique=bout_obj)
		fav_bout_obj = Boutique_favourite.objects.filter(boutique=bout_obj)
		print fav_bout_obj

		context['Products'] = pro_obj
		context['Fav_boutique'] = fav_bout_obj

		return context


# class boutique_detail_view(DetailView):

# 	model = Boutique
# 	product_form_class = ProductForm
# 	image_form_class = Product_image_form
# 	context_object_name = "boutique"
# 	template_name = "dashboard/boutique/boutique_details.html"

# 	def get_context_data(self, **kwargs):

# 		context = super(boutique_detail_view, self).get_context_data(**kwargs)
# 		context['form_product'] = self.product_form_class
# 		context['form_image'] = self.image_form_class
# 		return context

# 	def post(self, request, **kwargs):

# 		bout_id = kwargs['pk']
# 		bout_obj = Boutique.objects.get(id=bout_id)
# 		form_product = ProductForm(request.POST).save(commit=False)
# 		form_product.boutique = bout_obj
# 		discount = form_product.discount
		
# 		"""Checking whether there is any discount applied to this product 
# 		and if there is discount applied, discount amount is calculated and saved"""
# 		if discount != 0:
# 			price = form_product.price
# 			discount_amount = price - (price*discount/100)
# 			form_product.discount_amount = discount_amount

# 		form_product.save()
# 		form_image = Product_image_form(request.POST,request.FILES)
# 		if form_image.is_valid():
# 			form_image = form_image.save()
# 			form_product.image.add(form_image)
# 		return HttpResponseRedirect(reverse_lazy('dashboard:product-list'))


# Edit details view of Boutiques
class boutique_edit_view(UpdateView):

	model = Boutique
	template_name = "dashboard/boutique/boutique_edit.html"
	success_url = reverse_lazy('dashboard:boutique-list')

	def post(self, request, **kwargs):

		context = super(boutique_edit_view, self).post(request,**kwargs)
		messages.success(request, "Updated boutique")
		return context



class delete_size(TemplateView):
	template_name = "index.html"

	def get(self, request, **kwargs):

		bout_id = kwargs["pk"]

		if bout_id:
			try:
				size = Product_size1.objects.get(id=bout_id)
				size.delete()
				return HttpResponse("1") 
			except Exception, e:
				return HttpResponse("-1")
		else:
			return HttpResponse("-2") 



class delete_image(TemplateView):
	template_name = "index.html"

	def get(self, request, **kwargs):

		bout_id = kwargs["pk"]
		
		if bout_id:
			try:
				size = Product_image1.objects.get(id=bout_id)
				size.delete()
				messages.success(request, "Item deleted")
			except Exception, e:
				messages.errors(request, "Item does not exist")
		else:
			messages.errors(request, "Missing args")

		return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))





def upload(request):
	c={}

	if request.method == 'POST':
		
		ims = request.session['im']

		image = request.FILES.get('file', 'unknown.jpg')
		form = Product_image_form(request.POST, request.FILES)
		im = Product_image1(image=image)
		im.save()

		ims.append(im.id)
		request.session['im'] = ims	
		print ims
	else:
		return HttpResponse('Not OK')

	return HttpResponse('OK')



# This view edit's the products
class product_add(TemplateView):
	
	template_name = "dashboard/boutique/product_add.html"
	success_url = reverse_lazy('dashboard:boutique-list')


	def post(self, request, *args, **kwargs):

		QuoteItemFormFormset = formset_factory(Product_size_form)

		bout_id = kwargs["bout_id"]
		boutique = Boutique.objects.get(id=bout_id)

		product_form = ProductForm(request.POST)
		if product_form.is_valid():
			obj = product_form.save(commit=False)
		else:
			messages.error(request, "Please Fill all fields!")
			return self.get(self, request, *args, **kwargs)


		obj.boutique = boutique
		discount = obj.discount
		
# 		"""Checking whether there is any discount applied to this product 
# 		and if there is discount applied, discount amount is calculated and saved"""
		if discount != 0:
			price = obj.price
			discount_amount = price - (price*discount/100)
			obj.discount_amount = discount_amount

		obj.save()

		product_image_form = Product_image_form(request.POST, request.FILES)
		product_image_form.save()

		product_size_formset = QuoteItemFormFormset(request.POST)

		print "formset=",product_size_formset

		if product_size_formset.is_valid():
			for form in product_size_formset.forms:
				obj = form.save()
				obj.save()
				product_form.instance.size.add(obj)
				product_form.save()
		else:
			print "Invalid formset"

		images = self.request.session['im']
		print images
		for img in images:
			product_form.instance.image.add(img)

		request.session['im'] = []

		messages.success(request, "New product has been added")

		return redirect(self.success_url)


	def get_context_data(self, **kwargs):

		self.request.session['im'] = []

		context = super(product_add, self).get_context_data(**kwargs)

		product_form = ProductForm()
		product_image_form = Product_image_form()
		product_size_form = Product_size_form()
		QuoteItemFormFormset = formset_factory(Product_size_form)

		context['form'] = product_form
		context['image_form'] = product_image_form
		context['size_form'] = QuoteItemFormFormset

		return context



# This view edit's the products
class product_edit(TemplateView):
	
	# model = Product
	# fields = ['category', 'name', 'price', 'description', 'tags', 'discount','image','featured']
	template_name = "dashboard/boutique/product_edit.html"
	success_url = reverse_lazy('dashboard:boutique-list')


	def post(self, request, *args, **kwargs):

		product_id = kwargs['pk']
		product = Product.objects.get(id=product_id)

		product_form = ProductForm(request.POST, instance=product)
		obj = product_form.save(commit=False)

		discount = obj.discount
		
# 		"""Checking whether there is any discount applied to this product 
# 		and if there is discount applied, discount amount is calculated and saved"""
		if discount != 0:
			price = obj.price
			discount_amount = price - (price*discount/100)
			obj.discount_amount = discount_amount

		obj.save()

		product_image_form = Product_image_form(request.POST, request.FILES)
		product_image_form.save()

		sizes = product.size.all()

		QuoteItemFormFormset = modelformset_factory(Product_size1, extra=0)
		product_size_formset = QuoteItemFormFormset(request.POST, queryset=sizes)


		if product_size_formset.is_valid():
			for form in product_size_formset.forms:
				obj = form.save(commit=False)
				obj.save()
				product_form.instance.size.add(obj)

		else:
			print "Invalid formset"

		images = self.request.session['im']
		print images
		for img in images:
			product_form.instance.image.add(img)

		request.session['im'] = []

		messages.success(request, "Product has been saved")

		return redirect(self.success_url)



	def get_context_data(self, **kwargs):

		self.request.session['im'] = []

		context = super(product_edit, self).get_context_data(**kwargs)

		product_id = kwargs['pk']
		product = Product.objects.get(id=product_id)

		product_form = ProductForm(instance=product)
		product_image_form = Product_image_form()

		# QuoteItemFormFormset = formset_factory(Product_size_form, extra=0)
		# QuoteItemFormFormset = modelformset_factory(Product_size1)
		QuoteItemFormFormset = modelformset_factory(Product_size1, extra=0)

		sizes = []
		for i in product.size.all():
			c = {}
			c['id'] = i.id
			c['product_size'] = i.product_size
			c['stock'] = i.stock
			sizes.append(c)

		product_size_form = QuoteItemFormFormset(queryset=product.size.all())

		context['images'] = product.image.all()
		context['sizes'] = product.size.all()

		context['form'] = product_form
		context['image_form'] = product_image_form
		context['size_form'] = product_size_form

		return context


# This view edit's the products
class product_list(ListView):
	
	model = Product
	context_object_name = "products"
	template_name = "dashboard/boutique/product_list.html"


# This view delete's the products
class product_delete(DeleteView):
	
	model = Product
	context_object_name = "product"
	template_name = "dashboard/boutique/product_delete.html"
	success_url = reverse_lazy('dashboard:boutique-list')


"""This view is for adding templates for boutiques"""
class ListTemplateView(ListView):
	"""docstring for AddTemplateView"""

	model = Templatepage
	context_object_name = "boutique_template"
	template_name = "dashboard/boutique/boutique_template_list.html"

	def post(self, request):
		
		template_name = request.POST.get('template_add','')

		if template_name != "":
			Templatepage(name=template_name).save()
			messages.success(request, "Added new template")
		else:
			messages.error(request, "No template name given")

		return redirect(reverse("dashboard:list-template"))


"""This view is for adding templates for boutiques"""
class EditTemplateView(View):
	"""docstring for EditTemplateView"""

	def post(self, request):
		
		template_id = request.POST.get('template_id','')
		template_name = request.POST.get('template_edit','')

		try:
			if template_id and template_name !="":

				temp_obj = Templatepage.objects.get(id=template_id)
				temp_obj.name = template_name
				temp_obj.save()
				messages.success(request, "Edited template name")
			else:
				messages.error(request, "No template name given")
		except ObjectDoesNotExist, e:
			messages.error(request, "No template found for editing. Please add new template")

		return redirect(reverse('dashboard:list-template'))


"""This view is for adding templates for boutiques"""
class DeleteTemplateView(DeleteView):
	"""docstring for AddTemplateView"""

	model = Templatepage
	template_name = "dashboard/boutique/boutique_template_delete.html"
	success_url = reverse_lazy('dashboard:list-template')



"""This view is for adding category"""
class AddCategoryView(View):
	"""docstring for AddCategoryView"""

	def post(self, request):
		
		category_name = request.POST.get('category_add','')

		if category_name != "":
			Category(name=category_name).save()
			messages.success(request, "Added new category")
		else:
			messages.error(request, "No category name given")

		return redirect(reverse("dashboard:boutique-list"))


"""This view is for adding category"""
class EditCategoryView(View):
	"""docstring for EditCategoryView"""

	def post(self, request):
		
		category_id = request.POST.get('category_id','')
		category_name = request.POST.get('category_edit','') 

		try:
			if category_id and category_name !="":

				temp_obj = Category.objects.get(id=category_id)
				temp_obj.name = category_name
				temp_obj.save()
				messages.success(request, "Edited category name")
			else:
				messages.error(request, "No category name given")
		except ObjectDoesNotExist, e:
			messages.error(request, "No category found for editing. Please add new category")

		return redirect(reverse('dashboard:boutique-list'))


"""This view is for adding templates for boutiques"""
class DeleteCategoryView(DeleteView):
	"""docstring for DeleteCategoryView"""

	model = Category
	template_name = "dashboard/boutique/category_delete.html"
	success_url = reverse_lazy('dashboard:boutique-list')
