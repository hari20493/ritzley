from django.conf.urls import url

from oscar.core.loading import get_class
from oscar.core.application import Application
from views import *


class BoutiqueDashboardApplication(Application):
    name = None
    default_permissions = ['is_staff', ]

    list_view = get_class('dashboard.boutique.views', 'boutique_list_view')
    add_view = get_class('dashboard.boutique.views', 'add_boutique_view')
    delete_view = get_class('dashboard.boutique.views', 'delete_boutique_view')
    detail_view = get_class('dashboard.boutique.views', 'boutique_detail_view')
    edit_view = get_class('dashboard.boutique.views', 'boutique_edit_view')
    product_edit = get_class('dashboard.boutique.views', 'product_edit')
    product_list = get_class('dashboard.boutique.views', 'product_list')
    product_delete = get_class('dashboard.boutique.views', 'product_delete')
    product_add = get_class('dashboard.boutique.views', 'product_add')

    delete_size = get_class('dashboard.boutique.views', 'delete_size')
    delete_image = get_class('dashboard.boutique.views', 'delete_image')

    list_template_view = get_class('dashboard.boutique.views', 'ListTemplateView')
    edit_template_view = get_class('dashboard.boutique.views', 'EditTemplateView')
    delete_template_view = get_class('dashboard.boutique.views', 'DeleteTemplateView')

    add_category_view = get_class('dashboard.boutique.views', 'AddCategoryView')
    edit_category_view = get_class('dashboard.boutique.views', 'EditCategoryView')
    delete_category_view = get_class('dashboard.boutique.views', 'DeleteCategoryView')

    def get_urls(self):
        urls = [
            url(r'^$', self.list_view.as_view(), name='boutique-list'),
            url(r'^add/$', self.add_view.as_view(), name='add-boutique'),
            url(r'^delete/(?P<pk>\d+)$', self.delete_view.as_view(), name='delete-boutique'),
            url(r'^details/(?P<pk>\d+)$', self.detail_view.as_view(), name='boutique-details'),
            url(r'^edit/(?P<pk>\d+)$', self.edit_view.as_view(), name='edit-boutique-details'),
            url(r'^edit_product/(?P<pk>\d+)$', self.product_edit.as_view(), name='edit-product'),
            url(r'^add_product/(?P<bout_id>\d+)$', self.product_add.as_view(), name='add-product'),
            url(r'^product/$', self.product_list.as_view(), name='product-list'),
            url(r'^delete_product/(?P<pk>\d+)$', self.product_delete.as_view(), name='delete-product'),
            url(r'^delete_size/(?P<pk>\d+)$', self.delete_size.as_view(), name='delete-size'),
            url(r'^delete_image/(?P<pk>\d+)$', self.delete_image.as_view(), name='delete-image'),
            url(r'^upload/$', upload, name='upload'),
            url(r'^list_template/$', self.list_template_view.as_view(), name='list-template'),
            url(r'^edit_template/$', self.edit_template_view.as_view(), name='edit-template'),
            url(r'^delete_template/(?P<pk>\d+)/$', self.delete_template_view.as_view(), name='delete-template'),

            url(r'^add_category/$', self.add_category_view.as_view(), name='add-category'),
            url(r'^edit_category/$', self.edit_category_view.as_view(), name='edit-category'),
            url(r'^delete_category/(?P<pk>\d+)/$', self.delete_category_view.as_view(), name='delete-common-category'),
        ]
        return self.post_process_urls(urls)


application = BoutiqueDashboardApplication()
