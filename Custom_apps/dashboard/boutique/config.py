from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class BoutiqueDashboardConfig(AppConfig):
    label = 'boutique_dashboard'
    name = 'oscar.apps.dashboard.boutique'
    verbose_name = _('Boutique dashboard')
