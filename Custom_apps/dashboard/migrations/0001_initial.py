# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Boutique',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(null=True, blank=True)),
                ('faq', models.TextField(null=True, blank=True)),
                ('refund_policy', models.TextField(null=True, blank=True)),
                ('terms_conditions', models.TextField(null=True, blank=True)),
                ('header_image', models.ImageField(default=b'Unknown.jpg', null=True, upload_to=b'', blank=True)),
                ('logo', models.ImageField(default=b'Unknown.jpg', null=True, upload_to=b'', blank=True)),
                ('placeholder_1', models.ImageField(default=b'Unknown.jpg', upload_to=b'', null=True, verbose_name=b'Placeholder 1', blank=True)),
                ('placeholder_2', models.ImageField(default=b'Unknown.jpg', upload_to=b'', null=True, verbose_name=b'Placeholder 2', blank=True)),
                ('location', models.CharField(max_length=50, null=True, blank=True)),
                ('featured', models.BooleanField(default=False)),
                ('url', models.CharField(default=b'ritzley.com', max_length=1024, null=True, blank=True)),
                ('fb_url', models.CharField(default=b'facebook.com', max_length=1024, null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Boutique_favourite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('boutique', models.ForeignKey(default=True, to='dashboard.Boutique')),
                ('user', models.ForeignKey(default=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField()),
            ],
            options={
                'verbose_name': 'Faq',
                'verbose_name_plural': 'Faqs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=50)),
                ('price', models.IntegerField(default=0, max_length=50)),
                ('description', models.TextField(max_length=1000, null=True, blank=True)),
                ('tags', models.CharField(max_length=1000, null=True, blank=True)),
                ('discount', models.IntegerField(default=0, null=True, blank=True)),
                ('discount_amount', models.IntegerField(default=0, null=True, blank=True)),
                ('stitching', models.CharField(default=b'Stitched', max_length=50, choices=[(b'Stitched', b'Stitched'), (b'Half-Stitched', b'Half-Stitched'), (b'Non-Stitched', b'Non-Stitched')])),
                ('added_date', models.DateTimeField(default=datetime.date(2015, 7, 7), auto_now=True, auto_now_add=True, verbose_name=b'date added')),
                ('featured', models.BooleanField(default=False)),
                ('boutique', models.ForeignKey(related_name='boutique', default=True, to='dashboard.Boutique')),
                ('category', models.ForeignKey(default=True, to='dashboard.Category')),
                ('color', models.ForeignKey(default=True, to='dashboard.Color')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product_image1',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(default=b'Unknown.jpg', upload_to=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product_size1',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_size', models.CharField(max_length=50)),
                ('stock', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Templatepage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='product',
            name='image',
            field=models.ManyToManyField(to='dashboard.Product_image1', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='size',
            field=models.ManyToManyField(to='dashboard.Product_size1', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='boutique',
            name='template',
            field=models.ForeignKey(default=False, to='dashboard.Templatepage'),
            preserve_default=True,
        ),
    ]
