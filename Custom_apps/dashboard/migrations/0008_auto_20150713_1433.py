# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0007_auto_20150713_0504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boutique',
            name='location',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='boutique',
            name='url',
            field=models.CharField(default=b'ritzley.com', max_length=1024),
            preserve_default=True,
        ),
    ]
