# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0002_tailorcategory_sub_category'),
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='boutique',
            name='tailor',
            field=models.ForeignKey(blank=True, to='tailor.Tailor', null=True),
            preserve_default=True,
        ),
    ]
