# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0009_auto_20150817_0735'),
    ]

    operations = [
        migrations.AddField(
            model_name='boutique',
            name='email',
            field=models.CharField(max_length=1024, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='added_date',
            field=models.DateTimeField(default=datetime.date(2015, 8, 20), auto_now=True, auto_now_add=True, verbose_name=b'date added'),
            preserve_default=True,
        ),
    ]
