# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_boutique_tailor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='added_date',
            field=models.DateTimeField(default=datetime.date(2015, 7, 8), auto_now=True, auto_now_add=True, verbose_name=b'date added'),
            preserve_default=True,
        ),
    ]
