from django.contrib import admin
from Custom_apps.dashboard.models import *

# Register your models here.
class Admin(admin.ModelAdmin):
    '''
        Admin View for Boutique
    '''
    list_display = ('__unicode__',)

admin.site.register(Boutique, Admin)
admin.site.register(Category)
admin.site.register(Product_size1)
admin.site.register(Product_image1)
admin.site.register(Product)
admin.site.register(Templatepage)
admin.site.register(Boutique_favourite)
admin.site.register(Color)



