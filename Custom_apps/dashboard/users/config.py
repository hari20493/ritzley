from oscar.apps.dashboard.users import config


class UsersDashboardConfig(config.UsersDashboardConfig):
    name = 'Custom_apps.dashboard.users'
