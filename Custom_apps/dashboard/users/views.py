from oscar.apps.dashboard.users.views import IndexView as CoreIndexView
from oscar.apps.dashboard.users.views import UserDetailView as CoreUserDetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages
from django.views.generic import View, TemplateView
import string, os
from django.core.mail import send_mail
from django.shortcuts import redirect
from .forms import UserForm, UserModelForm
from django.views.generic.detail import DetailView
from django.core.mail import EmailMessage
from django.template import RequestContext, Context, Template
from django.template.loader import get_template


# -------------------------------------------General Functions---------------------------------
def Mailer(subject, content, to, template_html):

	from_email = ""
	html = get_template(template_html)
	d = Context(content)
	html_content = html.render(d)

	try:
		email = EmailMessage(subject, html_content, 'support@ritzley.com', [to])
		email.content_subtype = "html"  # Main content is now text/html
		email.send()
	except Exception, e:
		pass


# Write your views here
"""This view subclasses the original user IndexView"""
class IndexView(CoreIndexView):
	"""docstring for IndexView"""

	template_name = 'dashboard/users/index.html'

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)
		return context


"""This view is for Adding users"""
class AddUserView(TemplateView):
	"""docstring for IndexView"""

	template_name = 'dashboard/users/add_user.html'
	form_class = UserForm

	def get_context_data(self, **kwargs):
	    context = super(AddUserView, self).get_context_data(**kwargs)
	    context['form'] = self.form_class
	    return context

	def post(self, request):
		
		first_name = request.POST.get('first_name','')
		last_name = request.POST.get('last_name','')
		email = request.POST.get('email','')
		password = request.POST.get('password','')

		user = User.objects.create_user(email, email, password)
		user.first_name = first_name
		user.last_name = last_name
		user.is_active = True
		user.is_staff = True
		user.is_superuser = True
		user.save()
		messages.success(request, "Added admin successfully")


		return redirect(reverse_lazy('dashboard:users-index'))


"""This view is for rendering details about specific user"""
class UserDetailView(CoreUserDetailView):
	"""docstring for UserDetailView"""
	
	template_name = "dashboard/users/detail.html"


"""This view Edits the user details"""
class UpdateUserView(TemplateView):
	"""docstring for EditUserView"""
	
	template_name = "dashboard/users/update.html"

	def get_context_data(self, **kwargs):

	    context = super(UpdateUserView, self).get_context_data(**kwargs)
	    user_id = kwargs['pk']
	    user_obj = User.objects.get(id=user_id)
	    context['form'] = UserModelForm(instance=user_obj)
	    return context

	def post(self,request, **kwargs):
		
		user_id = kwargs['pk']

		first_name = request.POST.get('first_name','')
		last_name = request.POST.get('last_name','')
		email = request.POST.get('email','')
		password = request.POST.get('password','')

		user_obj = User.objects.get(id=user_id)

		if user_obj.first_name != first_name:
			user_obj.first_name = first_name

		if user_obj.last_name != last_name:
			user_obj.last_name = last_name

		if user_obj.email != email:
			user_obj.email = email

		if user_obj.password != password:
			user_obj.set_password(password)

		user_obj.save()

		return redirect(reverse_lazy('dashboard:users-index'))


"""This view Edits the user details"""
class DeleteUserView(DeleteView):
	"""docstring for EditUserView"""
	
	model = User
	template_name = "dashboard/users/delete.html"
	success_url = reverse_lazy('dashboard:users-index')


"""This view is for reseting password of users from dashboard via admin"""
class PasswordResetView(View):
	"""docstring for PasswordReset"""
	
	def post(self, request):

		email = request.POST.get('email', '')


		if email != '':

			try:
				u1 = User.objects.get(email = email)
			except Exception, e:
				messages.error(request, "User Does not Exist")
				c['error'] = "User Does not Exist"
				return render(request, 'iforgotthis.html', c)

			if u1:
				cid = u1.id
				print "user exists!"

				hash1 = generate_random_string(32, '0123456789ABCDEF')
				url = request.META['HTTP_HOST']

				link = "http://" + url + "/accounts/hasher/" + str(cid) +"/"+ str(hash1) +"/"
				print link

				content = {}
				content['user'] = u1
				content['url'] = link

				to = email
				subject = 'Account Password Reset'
				template_html = "reset_mail.html"

				Mailer(subject, content, to, template_html)

				messages.success(request, "Password Reset Mail sent to " + email)

		
		return redirect(reverse_lazy('dashboard:users-index'))
	


# -------------------------------------Functions----------------------------------------------------
"""Function for generating random string"""
def generate_random_string(length, stringset=string.ascii_letters+string.digits):
	'''
	Returns a string with `length` characters chosen from `stringset`
	>>> len(generate_random_string(20) == 20
	'''

	str = ''.join([stringset[i%len(stringset)] for i in [ord(x) for x in os.urandom(length)]])

	return str
