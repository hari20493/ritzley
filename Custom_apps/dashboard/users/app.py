from django.conf.urls import url
from oscar.core.loading import get_class
from oscar.apps.dashboard.users.app import UserManagementApplication as CoreUserManagementApplication



# Subclassing Dashboard.user app
class UserManagementApplication(CoreUserManagementApplication):

	add_user_view = get_class('dashboard.users.views', 'AddUserView')
	update_user_view = get_class('dashboard.users.views', 'UpdateUserView')
	delete_user_view = get_class('dashboard.users.views', 'DeleteUserView')
	password_reset_view = get_class('dashboard.users.views', 'PasswordResetView')

	def get_urls(self):
		urls = [
			url(r'^$', self.index_view.as_view(), name='users-index'),
			url(r'^add_user/$', self.add_user_view.as_view(), name='add-user'),
			url(r'^update_user/(?P<pk>\d+)/$', self.update_user_view.as_view(), name='update-user'),
			url(r'^delete_user/(?P<pk>\d+)/$', self.delete_user_view.as_view(), name='delete-user'),
			url(r'^(?P<pk>-?\d+)/$',
				self.user_detail_view.as_view(), name='user-detail'),
			url(r'^password-reset/$',
				self.password_reset_view.as_view(),
				name='user-password-reset'),

			# Alerts
			url(r'^alerts/$',
				self.alert_list_view.as_view(),
				name='user-alert-list'),
			url(r'^alerts/(?P<pk>-?\d+)/delete/$',
				self.alert_delete_view.as_view(),
				name='user-alert-delete'),
			url(r'^alerts/(?P<pk>-?\d+)/update/$',
				self.alert_update_view.as_view(),
				name='user-alert-update'),
		]
		return self.post_process_urls(urls)


application = UserManagementApplication()