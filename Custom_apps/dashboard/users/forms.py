from django import forms
from django.contrib.auth.models import User


# Create your forms here

class UserForm(forms.Form):
	# TODO: Define form fields here

	first_name = forms.CharField()
	last_name = forms.CharField()
	email = forms.EmailField()
	password = forms.CharField(widget=forms.PasswordInput)


class UserModelForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password']
        # widgets = {
        #     'password': forms.PasswordInput(),
        # }

