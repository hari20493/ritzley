from oscar.apps.customer.views import AccountAuthView as CoreAccountAuthView
from django.contrib.auth import logout 
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import View, TemplateView, RedirectView
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext, Context, Template
from django.contrib import auth
from django.core.context_processors import csrf 
from django.contrib.auth.forms import UserCreationForm
from Custom_apps.customer.forms import *

from django.template.loader import get_template

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

import datetime as dt
import json, string, os
import hashlib, random
import json
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.core.mail import send_mail
from django.contrib.auth.hashers import *
from django.core.urlresolvers import reverse, reverse_lazy

from django.db.models import Q, Sum

from django.utils.text import slugify

from Custom_apps.customer.models import *
from models import *

from django.contrib import messages
from django.core.mail import EmailMessage

# Functions

def Mailer(subject, content, to, template_html):

	from_email = ""
	html = get_template(template_html)
	d = Context(content)
	html_content = html.render(d)

	try:
		email = EmailMessage(subject, html_content, 'support@ritzley.com', [to])
		email.content_subtype = "html"  # Main content is now text/html
		email.send()
	except Exception, e:
		pass


def addProductToBag(user, product, chosen_size):

	if product.discount > 0:
		price = product.discount_amount
	else:
		price = product.price

	name = product.name
	type_of_item = "product"
	type_id = product.id

	images = product.image.all()

	if len(images) > 0:
		image = images.first().image.name
	else:
		image = "Unknown.jpg"

	category = product.category.name
	# cat = TailorCategory.objects.get(name__icontains=category)

	# print cat
	obj = Cart(user=user, image = image, name = name, price = price, type_of_item = type_of_item, type_id = type_id, size_id=chosen_size.id, product=product, chosen_size=chosen_size, stitching=product.stitching)
	obj.save()

	return obj.id


def getMyTotal(request):

	price = 0
	if request.user.is_authenticated():
		items = Cart.objects.filter(user = request.user)
		for item in items:
			if item.type_of_item == "product":
				price += item.price

			elif item.type_of_item == "stitching":
				prod_price = item.price
				stitching_cost = item.category.base_price
				pat_tot = item.patterns.all().aggregate(Sum('price'))
				pat_price = pat_tot['price__sum']

				price += stitching_cost + prod_price + pat_price

			elif item.type_of_item == "custom":
				if item.category is not None:
					stitching_cost = item.category.base_price
					tot = item.patterns.all().aggregate(Sum('price'))
					price += stitching_cost + tot['price__sum']

	return price


def getMyCartItems(request):

	items_json = []
	if request.user.is_authenticated():
		items = Cart.objects.filter(user = request.user)
		for item in items:
			c = {}

			if item.type_of_item == "product":

				c['id'] = item.id
				c['name'] = item.name
				c['type'] = item.type_of_item
				c['image'] = item.image.name
				c['tid'] = item.product.id
				c['price'] = item.price
				c['size'] = item.chosen_size.product_size
				c['url'] = item.product.boutique.url
				c['type'] = "product"
				c['date'] = str(dt.date.today())

			elif item.type_of_item == "stitching":

				base_price = item.category.base_price
				pat_sum = item.patterns.all().aggregate(Sum('price'))
				price = base_price + pat_sum['price__sum']

				c['id'] = item.id
				c['name'] = item.name
				c['type'] = item.type_of_item
				c['image'] = item.image.name
				c['tid'] = item.product.id
				c['price'] = item.price
				c['size'] = item.chosen_size.product_size
				c['url'] = item.product.boutique.url

				c['st_name'] = item.name
				c['st_price'] = price
				c['st_image'] = item.category.tailor.logo.name
				c['st_url'] = item.category.tailor.url
				c['type'] = "stitching"
				c['date'] = str(dt.date.today())

			elif item.type_of_item == "custom":

				if item.category is not None:

					base_price = item.category.base_price
					pat_sum = item.patterns.all().aggregate(Sum('price'))
					price = base_price + pat_sum['price__sum']

					c['st_price'] = price
					c['st_image'] = item.category.tailor.logo.name
					c['st_url'] = item.category.tailor.url
				else:
					c['st_url'] = "ritzley.com"
					c['st_image'] = "Unknown.jpg"
					c['st_price'] = "(process incomplete)"


				c['id'] = item.id
				c['type'] = item.type_of_item
				# c['name'] = item.name
				# c['image'] = item.image.name
				# c['tid'] = 0
				# c['price'] = item.price
				# c['size'] = 0
				# c['url'] = "#"

				c['st_name'] = item.name
				c['type'] = "custom"
				c['date'] = str(dt.date.today() + dt.timedelta(5))
				

			items_json.append(c)

	return items_json


# Create your views here.

class CartView(TemplateView):
	
	template_name = "cart.html"

	def get(self, request, *args, **kwargs):

		context = {}

		if request.user.is_authenticated():

			context['cart'] = getMyCartItems(request)
			tot = getMyTotal(request)
			print tot
			context['total'] = tot

		else:
			return HttpResponseRedirect('/')

		return render(request, self.template_name, context)

	def post(self, request, *args, **kwargs):
		context = {}

		if request.user.is_authenticated():

			flag = 0

			items = getMyCartItems(request)
			for item in items:
				print item
				if item['type'] == "custom":
					if item['st_price'] == "(process incomplete)":
						messages.error(request, "Please complete the process to proceed or remove the items from cart!")
						flag = 1
						break

			if flag == 0:
				return HttpResponseRedirect('/check_out')
			else:
				return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))



# Add a product to the cart
class CartAddToBag(View):

	def post(self, request, **kwargs):

		context = {}
		prod_id = kwargs["product_id"]
		size = request.POST.get('size-product', '')

		print prod_id, size

		if request.user.is_authenticated():

			if size and prod_id:
				# try:
				pdt = Product.objects.get(id=prod_id)
				chosen_size = pdt.size.get(id=size)

				print pdt, chosen_size

				# raise error is out of stock
				if chosen_size.stock == 0:
					messages.error(request, "Sorry, product " + str(pdt.name) + " of size "+ str(chosen_size.product_size)  +" is out of stock!")
					return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
				else:
					user = request.user
					ret = addProductToBag(user, pdt, chosen_size)

					messages.success(self.request, pdt.name + " Added to Cart")

				# except Exception, e:
					# messages.error(request, "Product Does not Exist")

				site = request.get_host()
				url1 = "http://" + site + "/" + slugify(pdt.name) + "_" + str(pdt.id)
				print url1

			else:
				messages.error(request, "Missing args!")
		else:
			return HttpResponseRedirect("/")

		# return redirect(url1)
		return redirect(request.META.get('HTTP_REFERER', '/'))




# Add a product to the cart
class CartAddToBagAjax(View):

	def get(self, request, **kwargs):

		context = {}
		prod_id = kwargs["product_id"]
		size = request.GET.get('size-product', '')

		print prod_id, size

		if request.user.is_authenticated():

			if size and prod_id:
				# try:
				pdt = Product.objects.get(id=prod_id)
				chosen_size = pdt.size.get(id=size)

				# raise error is out of stock
				if chosen_size.stock == 0:
					out = "-1" # no stock
				else:
					user = request.user
					ret = addProductToBag(user, pdt, chosen_size)
					self.request.session['cart_id'] = ret

					out = ret

				# except Exception, e:
					# messages.error(request, "Product Does not Exist")

			else:
				out = "-2" # missing args
		else:
			out = "-3" # access denied

		return HttpResponse(out)


# Delete any item from the cart
class CartDeleteFromBagAjax(View):

	def get(self, request, *args, **kwargs):

		context = {}
		prod_id = kwargs["item_id"]

		if request.user.is_authenticated():
			if prod_id:
				try:
					obj = Cart.objects.get(id=prod_id)
					obj.delete()
				except Exception, e:
					out = "-1"

				out = "1"
			else:
				out = "-2" # missing args
		else:
			out = "-3" # access denied

		return HttpResponse(out)


class CartDeleteFromBag(TemplateView):

	template_name = "cart.html"

	def get(self, request, *args, **kwargs):

		context = {}
		prod_id = kwargs["item_id"]

		if request.user.is_authenticated():
			try:
				obj = Cart.objects.get(id=prod_id)
				obj.delete()
			except Exception, e:
				messages.error(request, "Product Does not Exist")

			messages.success(request, obj.name + " Removed from Cart")

		return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))



class CartDeleteStitchingFromBag(TemplateView):

	template_name = "cart.html"

	def get(self, request, *args, **kwargs):

		context = {}
		prod_id = kwargs["item_id"]

		if request.user.is_authenticated():
			try:
				obj = Cart.objects.get(id=prod_id)
				obj.patterns.clear()
				obj.type_of_item = "product"
				obj.save()
				# obj.delete()
			except Exception, e:
				messages.error(request, "Product Does not Exist")

			messages.success(request, obj.name + " Stitching cost Removed from Cart")

		return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))



class CartDeleteStitchingFromBagAjax(View):

	def get(self, request, *args, **kwargs):

		context = {}
		prod_id = kwargs["item_id"]

		if request.user.is_authenticated():
			if prod_id:
				try:
					obj = Cart.objects.get(id=prod_id)
					obj.patterns.clear()
					obj.type_of_item = "product"
					obj.save()
				except Exception, e:
					out = "-1"

				out = "1"
			else:
				out = "-2" # missing args
		else:
			out = "-3" # access denied

		return HttpResponse(out)



class CheckoutView(TemplateView):
	"""View for Checkout Page"""

	template_name = "checkout.html"

	def get(self, request, *args, **kwargs):

		context = {}

		if request.user.is_authenticated():

			items_json = getMyCartItems(request)

			context['cart'] = items_json
			tot = getMyTotal(request)
			context['total'] = tot

			profile = UserProfile.objects.get(user = request.user)
			address = profile.address.all()

			context['addresses'] = address

			last = profile.address.filter(last_used=True)
			if len(last)>0:
				f = last[0]
				context['first'] = f.id
				context['current'] = Address.objects.get(id=f.id)

		return render(request, self.template_name, context)



	def post(self, request, *args, **kwargs):

		if request.user.is_authenticated():

			flag = 0
			items = getMyCartItems(request)
			for item in items:
				if item['type'] == "custom":
					if item['st_price'] == "(process incomplete)":
						messages.error(request, "Please complete the process to proceed or remove the items from cart!")
						flag = 1
						break

			if flag == 1:
				return HttpResponseRedirect('/cart/')
				

			ads = request.POST.get('address_radio', '0')
			profile = UserProfile.objects.get(user=request.user)

			try:
				address = Address.objects.get(id=ads)

			except Exception, e:
				messages.error(request, "Choose delivery Address")
				return self.get(request)

			all_ads =profile.address.all()
			for i in all_ads:
				i.last_used = False
				i.save()

			address.last_used = True
			address.save()

			items = Cart.objects.filter(user = request.user)
			for item in items:

				#check stock
				if item.type_of_item == "product":
					pdt = item.product
					chosen_size = item.chosen_size

					# raise error is out of stock
					if chosen_size.stock == 0:
						messages.error(request, "Sorry, product " + str(pdt.name) + "is out of stock!")
						return self.get(request)

				# insert into transaction table
				if item.type_of_item == "product":
					pdt = item.product
					chosen_size = item.chosen_size

					if chosen_size.stock > 0:
						today = dt.date.today()
						trans_obj = Transaction(user=request.user, size=chosen_size, product=pdt, amount=item.price, date_of_purchase=today, address=address)
						trans_obj.save()
						
						# Reduce stock by one
						chosen_size.stock = chosen_size.stock - 1
						chosen_size.save()

				elif item.type_of_item == "stitching":
					pdt = item.product
					chosen_size = item.chosen_size

					prod_price = item.price
					stitching_cost = item.category.base_price
					pat_tot = item.patterns.all().aggregate(Sum('price'))
					pat_price = pat_tot['price__sum']
					price = stitching_cost + prod_price + pat_price

					if chosen_size.stock > 0:
						today = dt.date.today()
						trans_obj = Transaction(user=request.user, size=chosen_size, product=pdt, amount=price, date_of_purchase=today, tailor=item.category.tailor, category=item.category, address=address)
						# Iterating patterns query set to add patterns
						trans_obj.save()
						for pattern in item.patterns.all():
							trans_obj.patterns.add(pattern)
						
						# Reduce stock by one
						chosen_size.stock = chosen_size.stock - 1
						chosen_size.save()

				elif item.type_of_item == "custom":

					stitching_cost = item.category.base_price
					tot = item.patterns.all().aggregate(Sum('price'))
					price = stitching_cost + tot['price__sum']

					today = dt.date.today()
					trans_obj = Transaction(user=request.user, amount=price, date_of_purchase=today, tailor=item.category.tailor, category=item.category, address=address)
					# Iterating patterns query set to add patterns
					trans_obj.save()
					for pattern in item.patterns.all():
						trans_obj.patterns.add(pattern)

			items_json_old = getMyCartItems(request)

			tot = getMyTotal(request)

			# send email
			to = request.user.email
			subject = "Purchase Order Confrimation"
			content = {}
			content['name'] = address.first_name + " " + address.last_name
			content['items'] = items_json_old
			content['total'] = tot
			content['address'] = address
			template_html = "purchase_confirm.html"

			Mailer(subject, content, to, template_html)

			


			# delete from cart
			request.session['cart_items'] = getMyCartItems(request)
			request.session['total_cart_items'] = 0


			request.session['cart_items_temp'] = getMyCartItems(request)
			tot_new = getMyTotal(request)
			request.session['total_cart_items_temp'] = tot_new
			request.session['delivery_address_temp'] = address.id
			Cart.objects.filter(user = request.user).delete()


		return redirect('/confirm_order/')



class OrderConfirmView(TemplateView):

	template_name = "confirm.html"

	def get_context_data(self, **kwargs):
		context = super(OrderConfirmView, self).get_context_data(**kwargs)

		if self.request.user.is_authenticated():
			if 'cart_items_temp' in self.request.session:
				if 'total_cart_items_temp' in self.request.session:
					if 'delivery_address_temp' in self.request.session:
						ads = self.request.session['delivery_address_temp']
						context['address'] = Address.objects.get(id=ads)
						context['items'] = self.request.session['cart_items_temp']
						context['total'] = self.request.session['total_cart_items_temp']
						
						del self.request.session['cart_items_temp']
						del self.request.session['total_cart_items_temp']
		else:
			redirect('/')

		return context



class SaveNewAddressView(TemplateView):

	template_name = "checkout.html"

	def post(self, request, *args, **kwargs):

		context = {}

		id1 = request.POST.get('a_id', '')
		first_name = request.POST.get('first_name', '')
		last_name = request.POST.get('last_name', '')
		# email = request.POST.get('email', '')
		mobile_number = request.POST.get('mobile_number', '')
		address_line_1 = request.POST.get('address_line_1', '')
		address_line_2 = request.POST.get('address_line_2', '')
		city = request.POST.get('city', '')
		postcode = request.POST.get('postcode', '')
		state = request.POST.get('state', '')
		country = request.POST.get('country', '')

		if request.user.is_authenticated():
			profile = UserProfile.objects.get(user = request.user)

			print id1

			if id1 == '':
				all_ads = profile.address.all()
				for i in all_ads:
					i.last_used = False
					i.save()

				new_address = Address(
					first_name=first_name, 
					last_name=last_name, 
					# email=email, 
					mobile_number=mobile_number, 
					address_line_1=address_line_1, 
					address_line_2=address_line_2, 
					city=city, 
					postcode=postcode,
					state=state,
					country=country,
					last_used = True
					)

				new_address.save()
				profile.address.add(new_address)
			else:

				sel_id = Address.objects.get(id = id1)

				all_ads =profile.address.all()
				for i in all_ads:
					i.last_used = False
					i.save()

				sel_id.first_name = first_name
				sel_id.last_name = last_name
				sel_id.mobile_number = mobile_number
				sel_id.address_line_1 = address_line_1
				sel_id.address_line_2 = address_line_2
				sel_id.city = city
				sel_id.postcode = postcode
				sel_id.state = state
				sel_id.country = country
				sel_id.last_used = True

				sel_id.save()


		return HttpResponseRedirect(reverse('check_out'))



class DeleteBillingAddress(TemplateView):

	template_name = "checkout.html"

	def get(self, request, *args, **kwargs):

		context = {}
		prod_id = kwargs["item_id"]

		if request.user.is_authenticated():
			try:
				obj = Address.objects.get(id=prod_id)

				if obj.primary == True:
					messages.error(request, "Primary Addresss cannot be removed")
				else:
					obj.delete()
					messages.success(request, "Removed Seleced Addresss")

			except Exception, e:
				messages.error(request, "Addresss does not Exist")

		return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))



class FindStitcherView(TemplateView):

	template_name = "product1.html"

	def dispatch(self, request, *args, **kwargs):
		bout_id = self.request.session['type_id']
		bout = Boutique.objects.get(id=bout_id)
		cart_id = self.request.session['cart_id']

		product_id = kwargs['product_id']
		prod = Product.objects.get(id=product_id)
		cat = prod.category.name

		if bout.tailor is not None:
			# tailor exists
			self.request.session['tailor_id'] = bout.tailor.id
			tailor_obj = Tailor.objects.get(id=bout.tailor.id)
			tailor_category_obj = TailorCategory.objects.filter(tailor=tailor_obj, name__icontains=cat)

			if len(tailor_category_obj) > 0:
				return redirect('/product_stitcher/'+str(cart_id)+"/")
			else:
				# no category 
				return redirect('http://www.ritzley.com/stitching/')
		else:
			#tailor does not exists
			return redirect('http://www.ritzley.com/stitching/')

		return super(FindStitcherView, self).dispatch(request, *args, **kwargs)


		

# Testing Views

class MailView(TemplateView):
	"""View for Checkout Page"""

	template_name = "purchase_confirm.html"

	def get(self, request, *args, **kwargs):

		context = {}

		if request.user.is_authenticated():

			items_json = []
			items = Cart.objects.all()
			for item in items:
				c = {}
				if item.type_of_item == "product":

					c['id'] = item.id
					c['name'] = item.name
					c['type'] = item.type_of_item
					c['image'] = item.image.name
					c['tid'] = item.product.id
					c['price'] = item.price
					c['size'] = item.chosen_size.product_size
					c['url'] = item.product.boutique.url

				elif item.type_of_item == "stitching":

					base_price = item.category.base_price
					pat_sum = item.patterns.all().aggregate(Sum('price'))
					price = base_price + pat_sum['price__sum']

					c['id'] = item.id
					c['name'] = item.name
					c['type'] = item.type_of_item
					c['image'] = item.category.tailor.logo.name
					c['tid'] = item.product.id
					c['price'] = price
					c['size'] = item.chosen_size.product_size
					c['url'] = item.category.tailor.url

					c['st_name'] = item.name
					c['st_price'] = price
					c['st_image'] = item.category.tailor.logo.name
					c['st_url'] = item.category.tailor.url

				elif item.type_of_item == "custom":

					base_price = item.category.base_price
					pat_sum = item.patterns.all().aggregate(Sum('price'))
					price = base_price + pat_sum['price__sum']

					c['id'] = item.id
					c['type'] = item.type_of_item
					c['name'] = item.name
					# c['image'] = item.image.name
					# c['tid'] = 0
					c['price'] = price
					# c['size'] = 0
					# c['url'] = "#"

					c['st_name'] = item.name
					c['st_price'] = price
					c['st_image'] = item.image.name
					c['st_url'] = item.category.tailor.url

				items_json.append(c)

				print items_json

			context['items'] = items_json
			tot = Cart.objects.all().aggregate(Sum('price'))
			context['total'] = tot['price__sum']

			profile = UserProfile.objects.latest('id')
			address = Address.objects.latest('id')

			context['user'] = profile
			context['address'] = address

		return render(request, self.template_name, context)




class SignupView(View):
	"""View for Checkout Page"""

	template_name = "signup_mail.html"

	def get(self, request, *args, **kwargs):

		context = {}

		if request.user.is_authenticated():

			context['user'] = request.user

		return render(request, self.template_name, context)




class ResetView(View):
	"""View for Checkout Page"""

	template_name = "reset_mail.html"

	def get(self, request, *args, **kwargs):

		context = {}

		if request.user.is_authenticated():

			context['user'] = request.user

		return render(request, self.template_name, context)



"""This view is to render special cart page for showing selected tailor patterns and its stitching cost"""
class PatternsCartView(TemplateView):
	"""docstring for PatternsCartView"""

	template_name = "pattern_cart.html"

	# @method_decorator(login_required())
	# def dispatch(self, request, *args, **kwargs):

	# 	# try:
	# 	# 	cart_obj = Cart.objects.get(id=cart_id)
	# 	# except:
	# 	# 	return redirect("/")

	# 	return super(PatternsCartView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):

		context = super(PatternsCartView, self).get_context_data(**kwargs)
		patterns = self.request.session['requested_patterns']
		cart_id = self.request.session['product_id']
		category_id = self.request.session['category_id']
		cart_obj = Cart.objects.get(id=cart_id)
		if cart_obj.product:
			prod_obj = Product.objects.get(id=cart_obj.product.id)
			context['product_content'] = prod_obj

		pattern_obj = TailorPatterns.objects.none()

		for pat_id in patterns:
			pattern_obj = pattern_obj | TailorPatterns.objects.filter(id=pat_id)

		# Finding total cost for stitching
		tailor_category_obj = TailorCategory.objects.get(id=category_id)
		basic_cost = tailor_category_obj.base_price
		total = 0
		for pat in pattern_obj:
			total += pat.price

		# Adds tailor basic cost as final amount
		total += basic_cost

		context['pattern_content'] = pattern_obj
		context['total'] = total
		context['basic_cost'] = basic_cost

		return context

	def post(self, request):
		
		patterns = self.request.session['requested_patterns']
		cart_id = self.request.session['product_id']
		category_id = self.request.session['category_id']
		cart_obj = Cart.objects.get(id=cart_id)

		pattern_obj = TailorPatterns.objects.none()

		for pat_id in patterns:
			pattern_obj = pattern_obj | TailorPatterns.objects.filter(id=pat_id)

		for patns in pattern_obj:
			cart_obj.patterns.add(patns)

		# Finding total cost for stitching
		tailor_category_obj = TailorCategory.objects.get(id=category_id)
		basic_cost = tailor_category_obj.base_price
		total = 0
		for pat in pattern_obj:
			total += pat.price

		# Adds tailor basic cost as final amount
		total += basic_cost
		tailor_obj = tailor_category_obj.tailor
		cart_obj.stitching_price = total

		if 'custom' not in request.session:
			cart_obj.type_of_item = "stitching"
		else:
			cart_obj.type_of_item = "custom"
			del request.session['custom']

		cart_obj.category = tailor_category_obj
		cart_obj.status = "complete"
		cart_obj.save()

		# # Destroying session data after usage
		# del self.request.session['requested_patterns']
		# del self.request.session['product_id']

		return redirect("/cart/")


"""This view for i have a material option. Using this view a new virtual product is added to cart """
class AddVirtualCartItem(View):
	"""docstring for AddVirtualCartItem"""
	
	def post(self, request):
		
		category_id = request.session['category_id']

		# Checks if user didn't select any category
		if category_id == "-1":
			messages.error(request, "Please select a category for stitching")
			return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
		else:
			try:
				tailor_category_obj = TailorCategory.objects.get(id=category_id)
				category = tailor_category_obj.name
				cart_obj = Cart(user=request.user, type_of_item="custom", status="complete", name=category)
				cart_obj.save()
			except ObjectDoesNotExist, e:
				messages.error(request, "Service does not exist, Please select another Tailor")
				return redirect("/")

		return redirect(reverse('promotions:product-stitching', kwargs={'id':cart_obj.id}))


# """This is ajax api view for fetching data from Transaction table"""
# class TransactionDetailsApiView(View):
# 	"""docstring for TransactionDetailsApiView"""
	
# 	def get(self, request):
		
# 		context = {}
# 		# template_name = "dashboard/transaction_details.html"
# 		template_name = "dashboard/transaction_details.html"
# 		trans_id = request.GET.get("tran_id")
# 		tran_obj = Transaction.objects.get(id=trans_id)

# 		# Calculate stitching cost
		

# 		context['trans_obj'] = tran_obj
# 		html = get_template(template_name)
# 		data = Context(context)
# 		html_content = html.render(data)

# 		return HttpResponse(html_content)


"""This is transaction details rendering view"""
class TransactionDetailsApiView(TemplateView):
	"""docstring for TransactionDetailsApiView"""

	template_name = "dashboard/transaction_details.html"
	
	def get_context_data(self, **kwargs):

		context = super(TransactionDetailsApiView, self).get_context_data(**kwargs)
		trans_id = self.request.GET.get("tran_id")
		tran_obj = Transaction.objects.get(id=trans_id)

		stitching_cost = 0
		# Calculate stitching cost
		if tran_obj.category:
			stitching_cost = tran_obj.category.base_price
			for pattern in tran_obj.patterns.all():
				stitching_cost += pattern.price

			context['stitching_cost'] = stitching_cost

		if tran_obj.product:
			if tran_obj.product.discount_amount != 0:
				total_cost = tran_obj.product.discount_amount
				if stitching_cost:
					total_cost += stitching_cost
			else:
				total_cost = tran_obj.product.price
				if stitching_cost:
					total_cost += stitching_cost
			context['total_cost'] = total_cost

		measure_obj = Measurements.objects.get(user=tran_obj.user)
		context['measurement'] = MeasurementsForm2(instance=measure_obj)

		user_profile_obj = UserProfile.objects.get(user=tran_obj.user)
		delivery_address_obj = user_profile_obj.address.get(last_used=True)
		context['delivery_address'] = UserAddressForm2(instance=delivery_address_obj)

		try:
			pickup_address_obj = user_profile_obj.address.get(pickup=True)
			context['pickup_address'] = UserAddressForm2(instance=pickup_address_obj)
		except:
			pass


		context['trans_obj'] = tran_obj
		return context



class ContinueProcessView(View):

    def get(self, request, **kwargs):

        context = {}

        request.session['custom'] = 1
        cart_id = kwargs['cid']

        return redirect('/product_stitcher/'+cart_id+'/')




