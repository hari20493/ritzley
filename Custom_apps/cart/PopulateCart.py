import re

from django.utils.text import compress_string
from django.utils.cache import patch_vary_headers

from django import http
from Custom_apps.dashboard.models import *

from django.http import HttpResponseRedirect, HttpResponsePermanentRedirect

from django.utils.text import slugify
from models import *
from django.db.models import Q, Sum
import json

def getMyTotal(request):

    price = 0
    if request.user.is_authenticated():
        items = Cart.objects.filter(user = request.user)
        for item in items:
            if item.type_of_item == "product":
                price += item.price

            elif item.type_of_item == "stitching":
                prod_price = item.price
                stitching_cost = item.category.base_price
                pat_tot = item.patterns.all().aggregate(Sum('price'))
                pat_price = pat_tot['price__sum']

                # print stitching_cost
                # print prod_price
                # print pat_price

                price += stitching_cost + prod_price + pat_price

            elif item.type_of_item == "custom":
                stitching_cost = item.category.base_price
                pat_tot = item.patterns.all().aggregate(Sum('price'))
                pat_price = pat_tot['price__sum']

                # print stitching_cost
                # print pat_price

                price += stitching_cost + pat_price

    return price


def getMyCartItems(request):

    items_json = []
    items = Cart.objects.filter(user = request.user)
    for item in items:
        c = {}

        if item.type_of_item == "product":

            c['id'] = item.id
            c['name'] = item.name
            c['type'] = item.type_of_item
            c['image'] = item.image.name
            c['tid'] = item.product.id
            c['price'] = item.price
            c['size'] = item.chosen_size.product_size
            c['url'] = item.product.boutique.url

        elif item.type_of_item == "stitching":

            base_price = item.category.base_price
            pat_sum = item.patterns.all().aggregate(Sum('price'))
            price = base_price + pat_sum['price__sum']

            c['id'] = item.id
            c['name'] = item.name
            c['type'] = item.type_of_item
            c['image'] = item.image.name
            c['tid'] = item.product.id
            c['price'] = item.price
            c['size'] = item.chosen_size.product_size
            c['url'] = item.product.boutique.url
            
            c['st_name'] = item.name
            c['st_price'] = price
            c['st_image'] = item.category.tailor.logo.name
            c['st_url'] = item.category.tailor.url

        elif item.type_of_item == "custom":

            base_price = item.category.base_price
            pat_sum = item.patterns.all().aggregate(Sum('price'))
            price = base_price + pat_sum['price__sum']

            c['id'] = item.id
            c['type'] = item.type_of_item
            # c['name'] = item.name
            # c['image'] = item.image.name
            # c['tid'] = 0
            # c['price'] = item.price
            # c['size'] = 0
            # c['url'] = "#"

            c['st_name'] = item.name
            c['st_price'] = price
            c['st_image'] = item.image.name
            c['st_url'] = item.category.tailor.url

        items_json.append(c)

    return items_json


class Carter(object):

    def process_request(self, request):

        try:
            if request.user.is_authenticated():
                
                request.session['cart_items'] = getMyCartItems(request)
                request.session['total_cart_items'] = getMyTotal(request)

                # print request.session['cart_items']

        except Exception, e:
            print "----------------------------------------------------------------------"
            print "Exception undu in cart populate!!! == "+str(e)
            print "----------------------------------------------------------------------"

        return None

