from oscar.apps.promotions.app import PromotionsApplication as CorePromotionsApplication
from django.conf.urls import url, include
from oscar.core.loading import get_class
from oscar.apps.promotions.models import PagePromotion, KeywordPromotion

from oscar.core.application import Application
from views import *


class CartApplication(Application):

    def get_urls(self):
        urls = [
            ## Cart Section

            url(r'^check_out/$', CheckoutView.as_view(), name="check_out"),

            url(r'^cart/$', CartView.as_view(), name="cart"),
            
            url(r'^cart/add_to_bag/(?P<product_id>\d+)/$', CartAddToBag.as_view(), name="add_to_bag"),
            url(r'^cart/delete/(?P<item_id>\d+)/$', CartDeleteFromBag.as_view(), name="cart_delete"),
            url(r'^cart/delete_stitching/(?P<item_id>\d+)/$', CartDeleteStitchingFromBag.as_view(), name="cart_delete_stitching"),

            url(r'^cart/add_to_bag_ajax/(?P<product_id>\d+)/$', CartAddToBagAjax.as_view(), name="add_to_bag_ajax"),
            url(r'^cart/delete_ajax/(?P<item_id>\d+)/$', CartDeleteFromBagAjax.as_view(), name="cart_delete_ajax"),
            url(r'^cart/delete_stitching_ajax/(?P<item_id>\d+)/$', CartDeleteStitchingFromBagAjax.as_view(), name="cart_delete_stitching_ajax"),

            url(r'^addnewaddress/$', SaveNewAddressView.as_view(), name="addnewaddress"),
            url(r'^delete_address/(?P<item_id>\d+)/$', DeleteBillingAddress.as_view(), name="deleteaddress"),
            url(r'^transaction_detail_api/$', TransactionDetailsApiView.as_view(), name="transaction-detail-api"),

            url(r'^confirm_order/$', OrderConfirmView.as_view(), name="order_confirm"),

            url(r'^cart/find_stitcher/(?P<product_id>\d+)/$', FindStitcherView.as_view(), name="cart"),
            url(r'^cart/continue/(?P<cid>\d+)/$', ContinueProcessView.as_view(), name="cart-continue"),
            
            url(r'^confirm_stitching/$', PatternsCartView.as_view(), name="pattern-cart"),
            url(r'^add_virtual_item/$', AddVirtualCartItem.as_view(), name="add-virtual-item"),

            # test urls for mail templates, can be removed
            url(r'^tester/$', MailView.as_view(), name="mailertest"),
            url(r'^signuptest/$', SignupView.as_view(), name="signuptest"),
            url(r'^resettest/$', ResetView.as_view(), name="resettest"),

        ]
        return self.post_process_urls(urls)


application1 = CartApplication()


