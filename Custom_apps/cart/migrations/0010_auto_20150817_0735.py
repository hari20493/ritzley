# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0012_address_pickup'),
        ('cart', '0009_auto_20150713_0504'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='address',
            field=models.ForeignKey(blank=True, to='customer.Address', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cart',
            name='added_date',
            field=models.DateField(auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_modified',
            field=models.DateField(auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_of_purchase',
            field=models.DateField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
