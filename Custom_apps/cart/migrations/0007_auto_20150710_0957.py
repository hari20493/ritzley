# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0005_tailorcategory_base_price'),
        ('cart', '0006_cart_stitching_price'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='pattern',
        ),
        migrations.AddField(
            model_name='cart',
            name='category',
            field=models.ForeignKey(blank=True, to='tailor.TailorCategory', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='transaction',
            name='category',
            field=models.ForeignKey(blank=True, to='tailor.TailorCategory', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='transaction',
            name='patterns',
            field=models.ManyToManyField(to='tailor.TailorPatterns', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cart',
            name='added_date',
            field=models.DateField(default=datetime.date(2015, 7, 10), auto_now=True, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_modified',
            field=models.DateField(default=datetime.date(2015, 7, 10), auto_now=True, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_of_purchase',
            field=models.DateField(default=datetime.date(2015, 7, 10)),
            preserve_default=True,
        ),
    ]
