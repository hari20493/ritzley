# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0010_auto_20150817_0735'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='status',
            field=models.CharField(default=b'order_processing', max_length=100, null=True, blank=True, choices=[(b'order_processing', b'Order processing'), (b'sent_to_tailor', b'Sent to tailor'), (b'tailoring_over', b'Tailoring over'), (b'shipped_to_customer', b'Shipped to customer'), (b'delivered', b'Delivered'), (b'canceled', b'Order Canceled')]),
            preserve_default=True,
        ),
    ]
