# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0005_auto_20150709_0015'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='stitching_price',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
