# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0007_auto_20150710_0957'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='added_date',
            field=models.DateField(default=datetime.date(2015, 7, 11), auto_now=True, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='cart',
            name='status',
            field=models.CharField(max_length=50, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_modified',
            field=models.DateField(default=datetime.date(2015, 7, 11), auto_now=True, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_of_purchase',
            field=models.DateField(default=datetime.date(2015, 7, 11)),
            preserve_default=True,
        ),
    ]
