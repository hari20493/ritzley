# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(default=b'Unknown.jpg', upload_to=b'')),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('price', models.IntegerField(default=0)),
                ('added_date', models.DateField(default=datetime.date(2015, 7, 7), auto_now=True, auto_now_add=True)),
                ('type_of_item', models.CharField(max_length=50)),
                ('type_id', models.IntegerField(default=0)),
                ('size_id', models.IntegerField(default=0)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField(default=0)),
                ('date_of_purchase', models.DateField(default=datetime.date(2015, 7, 7))),
                ('date_modified', models.DateField(default=datetime.date(2015, 7, 7), auto_now=True, auto_now_add=True)),
                ('status', models.CharField(default=b'order_processing', max_length=100, null=True, blank=True, choices=[(b'order_processing', b'Order processing'), (b'sent_to_tailor', b'Sent to tailor'), (b'tailoring_over', b'Tailoring over'), (b'shipped_to_customer', b'Shipped to customer'), (b'delivered', b'Delivered')])),
                ('product', models.ForeignKey(blank=True, to='dashboard.Product', null=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
