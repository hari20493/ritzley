# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0002_auto_20150707_1210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='added_date',
            field=models.DateField(default=datetime.date(2015, 7, 8), auto_now=True, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_modified',
            field=models.DateField(default=datetime.date(2015, 7, 8), auto_now=True, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_of_purchase',
            field=models.DateField(default=datetime.date(2015, 7, 8)),
            preserve_default=True,
        ),
    ]
