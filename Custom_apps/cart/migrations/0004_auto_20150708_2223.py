# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tailor', '0004_tailorsubcategory_tailor'),
        ('dashboard', '0003_auto_20150708_0544'),
        ('cart', '0003_auto_20150708_0544'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='chosen_size',
            field=models.ForeignKey(blank=True, to='dashboard.Product_size1', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cart',
            name='patterns',
            field=models.ManyToManyField(to='tailor.TailorPatterns', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cart',
            name='product',
            field=models.ForeignKey(blank=True, to='dashboard.Product', null=True),
            preserve_default=True,
        ),
    ]
