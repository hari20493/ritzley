from django.db import models
import datetime

from django.contrib.auth.models import User

from Custom_apps.dashboard.models import *
from Custom_apps.dashboard.tailor.models import *
from Custom_apps.customer.models import *

class Cart(models.Model):
	# Model for cart items
	# Stores all items linked along with user

	user = models.ForeignKey(User, null=True, blank=True)
	image = models.ImageField(default="Unknown.jpg")
	name = models.CharField(max_length=100, null=True, blank=True)
	price = models.IntegerField(default=0)
	stitching_price = models.IntegerField(default=0)
	added_date = models.DateField(auto_now_add=True)

	type_of_item = models.CharField(max_length=50)	# product, stitching, custom
	type_id = models.IntegerField(default=0)		# product_id
	size_id = models.IntegerField(default=0)		# size_id

	product = models.ForeignKey(Product, null=True, blank=True)
	chosen_size = models.ForeignKey(Product_size1, null=True, blank=True)
	patterns = models.ManyToManyField(TailorPatterns, null=True, blank=True)

	category = models.ForeignKey(TailorCategory, null=True, blank=True)

	status =  models.CharField(max_length=50, null=True, blank=True)
	stitching = models.CharField(max_length=50, null=True, blank=True)

	def __unicode__(self):
		return "{0} - {1} - {2}".format(self.id, self.name, self.price)


class Transaction(models.Model):
	# Model for storing transaction from COD

	STATUS = (
		('order_processing', 'Order processing'),
		('sent_to_tailor', 'Sent to tailor'),
		('tailoring_over', 'Tailoring over'),
		('shipped_to_customer', 'Shipped to customer'),
		('delivered', 'Delivered'),
		('canceled', 'Order Canceled'),
	)

	user = models.ForeignKey(User, null=True, blank=True)
	address = models.ForeignKey(Address, null=True, blank=True)
	
	product = models.ForeignKey(Product, null=True, blank=True)
	size = models.ForeignKey(Product_size1, null=True, blank=True)
	tailor = models.ForeignKey(Tailor, null=True, blank=True)
	patterns = models.ManyToManyField(TailorPatterns, null=True, blank=True)
	category = models.ForeignKey(TailorCategory, null=True, blank=True)
	amount = models.IntegerField(default=0)

	date_of_purchase = models.DateField(auto_now_add=True)
	date_modified = models.DateField(auto_now_add=True)

	status = models.CharField(max_length=100, null=True, blank=True, choices=STATUS, default='order_processing')


	def __unicode__(self):
		return "{0} - {1}".format(self.id, str(self.date_of_purchase))


