from oscar.apps.customer.views import AccountAuthView as CoreAccountAuthView
from django.contrib.auth import logout 
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import View, TemplateView, RedirectView
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib import auth
from django.core.context_processors import csrf 
from django.contrib.auth.forms import UserCreationForm
from forms import UserForm, MeasurementsForm

import json, string, os
import hashlib, random
import datetime

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.utils import timezone
from django.core.mail import send_mail
from django.contrib import messages
from django.contrib.auth.hashers import *
from django.core.urlresolvers import reverse, reverse_lazy

from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import RequestContext, Context, Template

from Custom_apps.customer.forms import *
from Custom_apps.customer.models import *

from Custom_apps.cart.models import *
from Custom_apps.dashboard.models import *

## General functions


def Mailer(subject, content, to, template_html):

	from_email = ""
	html = get_template(template_html)
	d = Context(content)
	html_content = html.render(d)

	try:
		email = EmailMessage(subject, html_content, 'support@ritzley.com', [to])
		email.content_subtype = "html"  # Main content is now text/html
		email.send()
	except Exception, e:
		pass


def g_mail(sub, body, to):

	try:
		email = EmailMessage(sub, body, 'info@bbis.com', to=to)
		email.content_subtype = "html"  # Main content is now text/html
		email.send()
	except Exception, e:
		pass

##


class AccountAuthView(CoreAccountAuthView):

	template_name = "index.html"

	def post(self, request):
		cont = {}
		username = request.POST.get('username')
		password = request.POST.get('password')
		keep = request.POST.get('keep')

		print keep

		user = authenticate(username=username, password=password)
		if user is not None:
			# the password verified for the user
			if user.is_active:
				login(request, user)

				if not keep:
					request.session.set_expiry(0)

				request.session['login_status'] = 1 
			else:
				messages.error(request, 'The password is valid, but the account has been disabled!')

		else:
			# the authentication system was unable to verify the username and password
			messages.error(request, 'The username and password were incorrect.')

		ret_url = request.META['HTTP_REFERER']

		return HttpResponseRedirect(ret_url)

		# return render(request, self.template_name, cont)


class getFBLogin(View):

	def get(self, request, **kwargs):
		#data = [{'name': user_name, 'id': user_id, 'e-mail': user_email}]

		fbid = kwargs['fbid']
		email = kwargs['email']
		first_name = kwargs['first_name']
		last_name = kwargs['last_name']
		name = kwargs['name']
		gender = kwargs['gender']

		if email:		
			try:
				# New fb user
				user = User.objects.create_user(email, email, "")
				user.first_name=first_name
				user.last_name=last_name
				user.save()

				activation_key = generate_random_string(64)
				username = email

				obj = UserProfile(user=user, activation_key=activation_key, username = username, password="")
				obj.save()

				user.backend = 'django.contrib.auth.backends.ModelBackend'
				login(request, user)
				request.session['fbsignup'] = 1
				return HttpResponse("2")

			except Exception, e:
				# Already regged user
				user = User.objects.get(username=email)
				if user.check_password(""):
					user.backend = 'django.contrib.auth.backends.ModelBackend'
					login(request, user)
					request.session['fbsignup'] = 1
					return HttpResponse("2")

			user.backend = 'django.contrib.auth.backends.ModelBackend'
			login(request, user)	
		else:
			# Ask user email
			return HttpResponse("-1")
			pass

		return HttpResponse("1")



class LogoutView(RedirectView):
	"""
	Provides users the ability to logout
	"""
	url = '/'

	def get(self, request, *args, **kwargs):
		logout(request)
		response = redirect('/')
		response.delete_cookie('sessionids')
		return response




class GetPassView(TemplateView):

	template_name = "set_pass.html"

	def dispatch(self, request, *args, **kwargs):

		if self.request.user.is_authenticated():
			if 'fbsignup' not in request.session:
				return redirect('/err404/')
		else:
			return redirect('/err404/')

		return super(GetPassView, self).dispatch(request, *args, **kwargs)

	def post(self, request, **kwargs):

		password = request.POST.get('password', '')

		if password != '':
			if self.request.user.is_authenticated():
				user = request.user
				user.set_password(password)
				user.save()
				messages.success(request, "Your Password has been set.")
				messages.success(request, "Please login again.")
				del request.session['fbsignup']
			else:
				messages.success(request, "Please login.")
				del request.session['fbsignup']
		else:
			messages.success(request, "Please fill password")
			return self.get(self, request, **kwargs)

		return HttpResponseRedirect('/')



# class UserRegistration(TemplateView):

# 	template_name = "index.html"

# 	def get_context_data(self, **kwargs):

# 		context = super(UserRegistration, self).get_context_data(**kwargs)
# 		context['form'] = MyRegistrationForm()
# 		return context

# 	def post(self,request):

# 		register = MyRegistrationForm(request.POST)
# 		myreg = register.save(commit=False)

# 		if register.is_valid():
# 			#username = request.POST.get('username')
# 			password = request.POST.get('password')
# 			email = request.POST.get('email')
# 			activation_key = generate_random_string(64)
			
# 			user = User.objects.create_user(email, email, password)
# 			myreg.user = user
# 			myreg.activation_key = activation_key
# 			myreg.save()

# 			obj = UserProfile(user=user, activation_key=activation_key, username = username, password="")
# 			obj.save()

# 			site_url = request.META['HTTP_HOST']

# 			email_subject = 'Account confirmation'
# 			email_body = "Hey \n, thanks for signing up. To activate your account, click this link within \
# 				48hours %s/accounts/confirm/%s" % (site_url,activation_key)

# 			send_mail(email_subject, email_body, 'maneeshvshaji@gmail.com', [email,], fail_silently=False)

# 		return HttpResponseRedirect('/')



class UserConfirm(TemplateView):
	
	template_name = "index.html"

	def get(self, request, *args, **kwargs):

		activation_key = kwargs['activation_key']
		usr = None

		try:
			usr = UserProfile.objects.get(activation_key=activation_key)
		except Exception, e:
			messages.error(request, "Invalid Activation Code. Please Signup again.")
			return HttpResponseRedirect("/")

		if usr is not None:

			import datetime

			key_expires = usr.key_expires.date()
			today = datetime.date.today()
			diff = today - key_expires

			print diff.days

			if diff.days < 2:
				user = usr.user
				user.backend = 'django.contrib.auth.backends.ModelBackend'
				login(request, user)
				request.session['login_status'] = 1

				messages.success(request, "Welcome to Ritzley. Your account has been activated.")
				return HttpResponseRedirect("/")
			else:
				messages.error(request, "Your Activation Code has expired. Please Signup again.")
				return HttpResponseRedirect("/")

		
		return super(UserConfirm, self).get(request, *args, **kwargs)



class Profile(TemplateView):
	
	template_name = "profile.html"

	def get_context_data(self, **kwargs):

		context = super(Profile, self).get_context_data(**kwargs)
		if self.request.user.is_authenticated():
			try:
				context['measurement'] = Measurements.objects.get(user=self.request.user)
			except Exception, e:
				pass

		return context

	def post(self, request):
		cont = {}

		one = request.POST.get('one', '')
		two = request.POST.get('two', '')
		three = request.POST.get('three', '')
		four = request.POST.get('four', '')
		five = request.POST.get('five', '')
		six = request.POST.get('six', '')
		seven = request.POST.get('seven', '')
		eight = request.POST.get('eight', '')
		nine = request.POST.get('nine', '')
		ten = request.POST.get('ten', '')
		eleven = request.POST.get('eleven', '')
		twelve = request.POST.get('twelve', '')
		thirteen = request.POST.get('thirteen', '')
		fourteen = request.POST.get('fourteen', '')
		fifteen = request.POST.get('fifteen', '')
		sixteen = request.POST.get('sixteen', '')
		seventeen = request.POST.get('seventeen', '')
		eighteen = request.POST.get('eighteen', '')


		# measurement = MeasurementsForm(request.POST)
		# mymesurement = measurement.save(commit=False)

		measurementfilter = Measurements.objects.filter(user=request.user)
		if len(measurementfilter) > 0:
			obj = Measurements.objects.get(user=request.user)
			obj.one = one
			obj.two = two
			obj.three = three
			obj.four = four
			obj.five = five
			obj.one = six
			obj.seven = seven
			obj.eight = eight
			obj.nine = nine
			obj.ten = ten
			obj.eleven = eleven
			obj.twelve = twelve
			obj.thirteen = thirteen
			obj.fourteen = fourteen
			obj.fifteen = fifteen
			obj.sixteen = sixteen
			obj.seventeen = seventeen
			obj.eighteen = eighteen

			obj.save()

		else:
			mymeasurement = Measurements(
				user=request.user,
				one = one,
				two = two,
				three = three,
				four = four,
				five = five,
				six = six,
				seven = seven,
				eight = eight,
				nine = nine,
				ten = ten,
				eleven = eleven,
				twelve = twelve,
				thirteen = thirteen,
				fourteen = fourteen,
				fifteen = fifteen,
				sixteen = sixteen,
				seventeen = seventeen,
				eighteen = eighteen)
				
			mymeasurement.save()

		return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))



class ProfileModal(TemplateView):
	
	template_name = "profile_modal.html"

	def get_context_data(self, **kwargs):

		context = super(ProfileModal, self).get_context_data(**kwargs)
		if self.request.user.is_authenticated():
			try:
				profile = UserProfile.objects.get(user=self.request.user)
				address = profile.address.get(primary = True)
				context['form'] = UserAddressForm(instance=address)
			except Exception, e:
				context['form'] = UserAddressForm()
				print str(e)
		else:
			return HttpResponseRedirect('/')

		return context


	def post(self, request, *args, **kwargs):

		context = {}

		if request.user.is_authenticated():
			profile = UserProfile.objects.get(user = request.user)

			try:
				address = profile.address.get(primary = True)
				if address is not None:

					ads = UserAddressForm(request.POST, instance=address)
					ads.save()

			except Exception, e:

				all_ads =profile.address.all()
				for i in all_ads:
					i.last_used = False
					i.primary = False
					i.save()

				ads = UserAddressForm(request.POST)
				new_address = ads.save()
				new_address.primary = True
				new_address.save()

				profile.address.add(new_address)

			messages.success(request, "Your Profile has been saved.")


		return HttpResponseRedirect(reverse('customer:profile'))



class MeasurementsClass(TemplateView):

	template_name = "measurements.html"

	def get_context_data(self, **kwargs):

		context = super(MeasurementsClass, self).get_context_data(**kwargs)
		context['my_form'] = MeasurementsForm()

		patterns = self.request.GET.getlist('pid',[])
		product = self.request.GET.get('pdt','')

		if self.request.user.is_authenticated():
			pat_session = []
			for pat in patterns:
				if pat != "undefined":
					pat_session.append(pat)

			print pat_session
			
			self.request.session['requested_patterns'] = pat_session
			self.request.session['product_id'] = product

			try:
				context['measurement'] = Measurements.objects.get(user=self.request.user)
			except Exception, e:
				pass

		return context


	def post(self, request):
		cont = {}

		one = request.POST.get('one', '')
		two = request.POST.get('two', '')
		three = request.POST.get('three', '')
		four = request.POST.get('four', '')
		five = request.POST.get('five', '')
		six = request.POST.get('six', '')
		seven = request.POST.get('seven', '')
		eight = request.POST.get('eight', '')
		nine = request.POST.get('nine', '')
		ten = request.POST.get('ten', '')
		eleven = request.POST.get('eleven', '')
		twelve = request.POST.get('twelve', '')
		thirteen = request.POST.get('thirteen', '')
		fourteen = request.POST.get('fourteen', '')
		fifteen = request.POST.get('fifteen', '')
		sixteen = request.POST.get('sixteen', '')
		seventeen = request.POST.get('seventeen', '')
		eighteen = request.POST.get('eighteen', '')


		# measurement = MeasurementsForm(request.POST)
		# mymesurement = measurement.save(commit=False)

		measurementfilter = Measurements.objects.filter(user=request.user)
		if len(measurementfilter) > 0:
			obj = Measurements.objects.get(user=request.user)
			obj.one = one
			obj.two = two
			obj.three = three
			obj.four = four
			obj.five = five
			obj.one = six
			obj.seven = seven
			obj.eight = eight
			obj.nine = nine
			obj.ten = ten
			obj.eleven = eleven
			obj.twelve = twelve
			obj.thirteen = thirteen
			obj.fourteen = fourteen
			obj.fifteen = fifteen
			obj.sixteen = sixteen
			obj.seventeen = seventeen
			obj.eighteen = eighteen

			obj.save()

		else:
			mymeasurement = Measurements(
				user=request.user,
				one = one,
				two = two,
				three = three,
				four = four,
				five = five,
				six = six,
				seven = seven,
				eight = eight,
				nine = nine,
				ten = ten,
				eleven = eleven,
				twelve = twelve,
				thirteen = thirteen,
				fourteen = fourteen,
				fifteen = fifteen,
				sixteen = sixteen,
				seventeen = seventeen,
				eighteen = eighteen)
				
			mymeasurement.save()

		return HttpResponseRedirect('/confirm_stitching/')



class ChangePass(TemplateView):

	template_name = "profile.html"

	def post(self,request):

		oldpassword = request.POST.get('opsw')
		newpassword = request.POST.get('npsw')

		if self.request.user.is_authenticated():
			user = request.user
			authpass = user.check_password(oldpassword)
			if authpass:
				user.set_password(newpassword)
				user.save()
				messages.success(request, "Your Password has been Updated.")
				messages.success(request, "Please login again.")
			else:
				messages.error(request, "Old password is incorrect")
				return HttpResponseRedirect(reverse('customer:profile'))
		else:
			return HttpResponseRedirect('/')

		return HttpResponseRedirect('/')





def generate_random_string(length, stringset=string.ascii_letters+string.digits):
	'''
	Returns a string with `length` characters chosen from `stringset`
	>>> len(generate_random_string(20) == 20
	'''

	str = ''.join([stringset[i%len(stringset)] for i in [ord(x) for x in os.urandom(length)]])

	return str




def reset(request):

	c={}

	if request.method == "POST":
		email = request.POST.get('email', '')

		if email != '':

			try:
				u1 = User.objects.get(email = email)
			except Exception, e:
				messages.error(request, "User Does not Exist")
				c['error'] = "User Does not Exist"
				return render(request, 'iforgotthis.html', c)

			if u1:
				cid = u1.id
				print "user exists!"

				hash1 = generate_random_string(32, '0123456789ABCDEF')
				url = request.META['HTTP_HOST']

				link = "http://" + url + "/accounts/hasher/" + str(cid) +"/"+ str(hash1) +"/"
				print link

				content = {}
				content['user'] = u1
				content['url'] = link

				to = email
				subject = 'Account Password Reset'
				template_html = "reset_mail.html"

				Mailer(subject, content, to, template_html)

				messages.success(request, "Success! Please Check your mail!")

				return HttpResponseRedirect("/")

		else:
			messages.error(request, "Please enter your email id!")

	return render(request, 'iforgotthis.html', c)


def hasher(request, cid, hash):

	c={}
	c.update(csrf(request))

	if request.method == 'POST':
		password = request.POST.get('password', '')

		#try:
		user = User.objects.get(pk=cid)
		user.set_password(password)
		user.save()
		#except User.DoesNotExist:
		#	pass

		try:
			m = UserProfile.objects.get(user=user)
			m.password = password
			m.save()
		except Exception, e:
			pass

		messages.success(request, 'Save Successful')
		return HttpResponseRedirect('/')
	else:

		user = User.objects.get(pk=cid)
		messages.success(request, user.username)
		
		return render(request, 'change_pass.html', c)



def signup(request):

	c={}
	c.update(csrf(request))

	if request.method == 'POST':
		first_name = request.POST.get('username', '')
		email = request.POST.get('email', '')
		password = request.POST.get('password', '')

		activation_key = generate_random_string(64)

		try:
			user = User.objects.create_user(email, email, password)
			user.first_name = first_name
			user.save()

			obj = UserProfile(user=user, activation_key=activation_key, password=password)
			obj.save()

			site_url = request.META['HTTP_HOST']
			url = "%s/accounts/confirm/%s" % (site_url,activation_key)

			content = {}
			content['user'] = user
			content['url'] = url

			to = email
			subject = 'Account confirmation'
			template_html = "signup_mail.html"

			Mailer(subject, content, to, template_html)

			messages.success(request, "Success! Please Check your mail to activate account!")

		except Exception, e:
			messages.error(request, "User Already Exists!")
			HttpResponseRedirect('/')


	return HttpResponseRedirect('/')
# Order history view class
class order_history(TemplateView):
	template_name = "orderHistory.html"
	def get_context_data(self, **kwargs):
		context = super(order_history, self).get_context_data(**kwargs)
		if self.request.user.is_authenticated:
			# user_obj = self.request.user
			username = self.request.user.username
			user_obj = User.objects.get(username=username)
			trn_obj = Transaction.objects.filter(user=user_obj)
			context['transaction'] = trn_obj

		return context

# Cancel order view class

class order_history_cancel(View):

	# template_name = "orderHistory.html"
	# @method_decorator(csrf_exempt)
	def get(self, request, *args, **kwargs):

		context = {}
		prod_id = kwargs["item_id"]
		if request.user.is_authenticated():
			try:
				obj = Transaction.objects.get(id=prod_id)
				size_id = obj.size.id
				obj.status = 'canceled'
				obj.save()
				# prod_size = Product_size1.objects.get(id = size_id)
				# latest_stock = prod_size.stock + 1
				# prod_size.stock = latest_stock
				# prod_size.save()
				obj.size.stock = obj.size.stock + 1
				obj.size.save()
			except Exception, e:
				messages.error(request, "Product Does not Exist")

			messages.success(request, obj.product.name + " Order Canceled")

		return  HttpResponseRedirect('/accounts/order-history/')




