from oscar.apps.customer.models import *  # noqa
from django.db import models
from django.contrib.auth.models import User
# import datetime
from datetime import datetime


class Address(models.Model):

	first_name = models.CharField( max_length=100, null=True, blank=True)
	last_name = models.CharField( max_length=100, null=True, blank=True)
	# email = models.CharField( max_length=255, null=True,  blank=True)
	mobile_number = models.CharField( max_length=20, null=True,  blank=True)
	address_line_1 = models.CharField(max_length=200, null=True,  blank=True)
	address_line_2 = models.CharField(max_length=200, null=True,  blank=True)
	city = models.CharField(max_length=100, null=True,  blank=True)
	postcode = models.CharField(max_length=50, null=True,  blank=True)
	# district = models.CharField(max_length=100, null=True,  blank=True)
	state = models.CharField(max_length=100, null=True,  blank=True)
	country = models.CharField(max_length=100, null=True,  blank=True)

	primary = models.BooleanField(default=False)
	last_used = models.BooleanField(default=False)
	pickup = models.BooleanField(default=False)

	class Meta:
		verbose_name = "Address"
		verbose_name_plural = "Addresses"

	def __unicode__(self):

		if self.primary:
			ret =  "{0} {1}'s primary address".format(self.first_name, self.last_name)
		else:
			if self.last_used:
				ret =  "{0} {1}'s last used address".format(self.first_name, self.last_name)
			else:
				ret =  "{0} {1}'s secondary address".format(self.first_name, self.last_name)

		return ret


class UserProfile(models.Model):

	user = models.OneToOneField(User)

	activation_key = models.CharField(max_length=100, null=True, unique=True)
	key_expires = models.DateTimeField(default=datetime.now)

	username = models.CharField(max_length=50, null=True, blank=True)
	password = models.CharField(max_length=100, null=True, blank=True)

	address = models.ManyToManyField(Address, null=True, blank=True)

	def __unicode__(self):
		return self.user.username


class Measurements(models.Model):

	user = models.ForeignKey(User)

	one = models.FloatField(max_length=7, null=True, blank=True)
	two = models.FloatField(max_length=7, null=True, blank=True)
	three = models.FloatField(max_length=7, null=True, blank=True)
	four = models.FloatField(max_length=7, null=True, blank=True)
	five = models.FloatField(max_length=7, null=True, blank=True)
	six = models.FloatField(max_length=7, null=True, blank=True)
	seven = models.FloatField(max_length=7, null=True, blank=True)
	eight = models.FloatField(max_length=7, null=True, blank=True)
	nine = models.FloatField(max_length=7, null=True, blank=True)
	ten = models.FloatField(max_length=7, null=True, blank=True)
	eleven = models.FloatField(max_length=7, null=True, blank=True)
	twelve = models.FloatField(max_length=7, null=True, blank=True)
	thirteen = models.FloatField(max_length=7, null=True, blank=True)
	fourteen = models.FloatField(max_length=7, null=True, blank=True)
	fifteen = models.FloatField(max_length=7, null=True, blank=True)
	sixteen = models.FloatField(max_length=7, null=True, blank=True)
	seventeen = models.FloatField(max_length=7, null=True, blank=True)
	eighteen = models.FloatField(max_length=7, null=True, blank=True)
	
	def __unicode__(self):
		return self.user.username




