from django import forms 
from django.contrib.auth.models import User
from django.contrib.auth import models
from Custom_apps.customer.models import *

from django.contrib.auth.forms import UserCreationForm


class UserForm(forms.ModelForm):

	class Meta:
		model = User
		fields = ('username','email', 'password')

class MeasurementsForm(forms.ModelForm):

	class Meta:
		model = Measurements
		fields = ('one','two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen','fifteen', 'sixteen', 'seventeen', 'eighteen')
	

class UserAddressForm(forms.ModelForm):

	class Meta:
		model = Address
		exclude = ('primary', 'last_used', 'pickup', )
		# widgets = {
		# 	'first_name': forms.TextInput(attrs={'readonly': 'readonly'}),
		# }

	def __init__(self, *args, **kwargs):
		super(UserAddressForm, self).__init__(*args, **kwargs)

		for key in self.fields.keys():
			self.fields[key].widget.attrs = {'class':'form-control', 'placeholder' : 'Enter ' + self.fields[key].label, "required" : "true"}

		# self.fields['first_name'].widget.attrs = {'class':'form-control', 'placeholder' : 'Enter first name', "required" : "true", "readonly" : "true"}


class UserAddressForm2(forms.ModelForm):

	class Meta:
		model = Address
		exclude = ('primary', 'last_used', 'pickup', )
		
	def __init__(self, *args, **kwargs):
		super(UserAddressForm2, self).__init__(*args, **kwargs)

		for key in self.fields.keys():
			self.fields[key].widget.attrs = {'class':'form-control', 'placeholder' : 'Enter ' + self.fields[key].label, "readonly" : "true"}


class MeasurementsForm2(forms.ModelForm):

	class Meta:
		model = Measurements
		fields = ('one','two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen','fifteen', 'sixteen', 'seventeen', 'eighteen')
	
	def __init__(self, *args, **kwargs):
		super(MeasurementsForm2, self).__init__(*args, **kwargs)

		for key in self.fields.keys():
			self.fields[key].widget.attrs = {'class':'form-control', 'placeholder' : 'Enter ' + self.fields[key].label, "readonly" : "true"}