from oscar.apps.customer.admin import *  # noqa
from .models import *

admin.site.register(UserProfile)
admin.site.register(Measurements)
admin.site.register(Address)
