from oscar.apps.customer.views import AccountAuthView as CoreAccountAuthView
from django.contrib.auth import logout 
from django.contrib.auth import authenticate, login
from django.shortcuts import render, HttpResponse
from django.views.generic import View, TemplateView
from django.contrib.auth.models import User
import json
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf 
from django.contrib.auth.forms import UserCreationForm
from forms import UserForm
import hashlib, datetime, random
from django.utils import timezone

class AccountAuthView(CoreAccountAuthView):

	template_name = "index.html"
	cont = {}
		
	def post(self, request):
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user is not None:
			# the password verified for the user
			if user.is_active:
				cont['response'] = "User is valid, active and authenticated"
				login(request, user)
				self.cont['auth'] = "User logged in"
			else:
				cont['response'] = "The password is valid, but the account has been disabled!"
		else:
			# the authentication system was unable to verify the username and password
			cont['response'] = "The username and password were incorrect."

		return render(request, self.template_name, self.cont)


class getFBLogin(View):

	def get(self, request, **kwargs):
		#data = [{'name': user_name, 'id': user_id, 'e-mail': user_email}]

		fbid = kwargs['fbid']
		email = kwargs['email']
		first_name = kwargs['first_name']
		last_name = kwargs['last_name']
		name = kwargs['name']
		gender = kwargs['gender']

		if email:		
			try:
				# New fb user
				user = User.objects.create_user(email, email, "")
				user.first_name=first_name
				user.last_name=last_name
				user.save()
			except Exception, e:
				# Already regged user
				user = User.objects.get(username=email)
			user.backend = 'django.contrib.auth.backends.ModelBackend'
			login(request, user)	
		else:
			# Ask user email
			pass

		return HttpResponse("OK")


class logout(View):

	def get(self, request):
		logout(request)
		return redirect('/')



class UserRegistration(TemplateView):

	template_name = "registers.html"

	def get_context_data(self, **kwargs):

		context = super(UserRegistration, self).get_context_data(**kwargs)
		context['form'] = UserForm
		return context

	def post(self, request):
		
		form = UserForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			email = form.cleaned_data['email']
			salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
			activation_key = hashlib.sha1(salt+email).hexdigest()            
			key_expires = datetime.datetime.today() + datetime.timedelta(2)

			#Get user by username
			user=User.objects.get(username=username)

			# Create and save user profile                                                                                                                                  
			new_profile = UserProfile(user=user, activation_key=activation_key, 
				key_expires=key_expires)
			new_profile.save()

			# Send email with activation key
			email_subject = 'Account confirmation'
			email_body = "Hey %s, thanks for signing up. To activate your account, click this link within \
			48hours http://127.0.0.1:8000/accounts/confirm/%s" % (username, activation_key)

			send_mail(email_subject, email_body, 'myemail@example.com',
				[email], fail_silently=False)

			return HttpResponseRedirect('/accounts/register_success')
		else:
			args['form'] = RegistrationForm()

		return render_to_response('user_profile/register.html', args, context_instance=RequestContext(request))