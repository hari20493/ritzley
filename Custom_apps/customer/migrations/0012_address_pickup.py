# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0011_remove_address_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='pickup',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
