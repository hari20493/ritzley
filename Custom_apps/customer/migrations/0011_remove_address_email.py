# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0010_auto_20150623_1502'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='address',
            name='email',
        ),
    ]
