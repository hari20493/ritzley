# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customer', '0003_auto_20150609_1621'),
    ]

    operations = [
        migrations.CreateModel(
            name='Measurements',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('one', models.FloatField(max_length=7, null=True, blank=True)),
                ('two', models.FloatField(max_length=7, null=True, blank=True)),
                ('three', models.FloatField(max_length=7, null=True, blank=True)),
                ('four', models.FloatField(max_length=7, null=True, blank=True)),
                ('five', models.FloatField(max_length=7, null=True, blank=True)),
                ('six', models.FloatField(max_length=7, null=True, blank=True)),
                ('seven', models.FloatField(max_length=7, null=True, blank=True)),
                ('eight', models.FloatField(max_length=7, null=True, blank=True)),
                ('nine', models.FloatField(max_length=7, null=True, blank=True)),
                ('ten', models.FloatField(max_length=7, null=True, blank=True)),
                ('eleven', models.FloatField(max_length=7, null=True, blank=True)),
                ('twelve', models.FloatField(max_length=7, null=True, blank=True)),
                ('thirteen', models.FloatField(max_length=7, null=True, blank=True)),
                ('fourteen', models.FloatField(max_length=7, null=True, blank=True)),
                ('fifteen', models.FloatField(max_length=7, null=True, blank=True)),
                ('sixteen', models.FloatField(max_length=7, null=True, blank=True)),
                ('seventeen', models.FloatField(max_length=7, null=True, blank=True)),
                ('eighteen', models.FloatField(max_length=7, null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='key_expires',
            field=models.DateTimeField(default=datetime.date(2015, 6, 13)),
            preserve_default=True,
        ),
    ]
