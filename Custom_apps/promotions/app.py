from oscar.apps.promotions.app import PromotionsApplication as CorePromotionsApplication
from django.conf.urls import url, include
from oscar.core.loading import get_class
from oscar.apps.promotions.models import PagePromotion, KeywordPromotion


class PromotionsApplication(CorePromotionsApplication):

    Filter = get_class('promotions.views', 'FilterProducts')
    Search = get_class('promotions.views', 'SearchProducts')
    AddToFav = get_class('promotions.views', 'AddToFav')
    DelToFav = get_class('promotions.views', 'DelToFav')
    PriceFiltering = get_class('promotions.views', 'PriceFiltering')
    MyProductDetailView = get_class('promotions.views', 'MyProductDetailView')
    Error404 = get_class('promotions.views', 'Error404')
    TailorView = get_class('promotions.views', 'TailorView')
    # SetCookieView = get_class('promotions.views', 'SetCookieView')
    FetchFaq = get_class('promotions.views', 'FetchFaq')
    StitchingCenterView = get_class('promotions.views', 'StitchingCenterView')
    ProductStitchingView = get_class('promotions.views', 'ProductStitchingView')
    PickUpFormView = get_class('promotions.views', 'PickUpFormView')

    def get_urls(self):
        urls = [
            # url(r'^boutique/(?P<boutique_slug>\w+)_(?P<id>\d+)/$', self.boutique_view.as_view(), name='boutique_view_page'),
            # url(r'^boutique/(?P<boutique_slug>.*)_(?P<id>\d+)/(?P<product_slug>.*)_(?P<pk>\d+)/$', self.MyProductDetailView.as_view(), name='Product-page-view'),
            url(r'^(?P<product_slug>.*)_(?P<pk>\d+)/$', self.MyProductDetailView.as_view(), name='Product-page-view'),
            url(r'page-redirect/(?P<page_promotion_id>\d+)/$',
                self.record_click_view.as_view(model=PagePromotion),
                name='page-click'),
            url(r'keyword-redirect/(?P<keyword_promotion_id>\d+)/$',
                self.record_click_view.as_view(model=KeywordPromotion),
                name='keyword-click'),
            url(r'^$', self.home_view.as_view(), name='home'),
            url(r'^filter/$', self.Filter.as_view(), name='Filter'),
            url(r'^searchp/$', self.Search.as_view(), name='Search'),
            url(r'^addtofav/$', self.AddToFav.as_view(), name='AddToFav'),
            url(r'^deltofav/$', self.DelToFav.as_view(), name='DelToFav'),
            url(r'^pricefilter/$', self.PriceFiltering.as_view(), name='PriceFiltering'),

            url(r'^err404/$', self.Error404.as_view(), name='Error404'),
            url(r'^stitching/$', self.TailorView.as_view(), name='TailorView'),
            # url(r'^setcookie/(?P<cookieid>.*)/$', self.SetCookieView.as_view(), name='SetCookieView'),
            url(r'^fetchfaq/$', self.FetchFaq.as_view(), name='FetchFaq'),
            url(r'^stitching_center/$', self.StitchingCenterView.as_view(), name='stitching-center'),
            url(r'^product_stitcher/(?P<id>\d+)/$', self.ProductStitchingView.as_view(), name='product-stitching'),
            url(r'^pick_up/$', self.PickUpFormView.as_view(), name='pick-up-form'),
        ]
        return self.post_process_urls(urls)


application = PromotionsApplication()


