from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from oscar.apps.promotions.views import HomeView as CoreHomeView
from Custom_apps.dashboard.models import Product, Product_image1, Faq
from Custom_apps.dashboard.models import Boutique, Boutique_favourite
from django.db.models import Q
from django.contrib import messages
from django.views.generic import View, TemplateView, ListView, DetailView
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist

from django.core.urlresolvers import reverse, reverse_lazy
import json
from django.utils.text import slugify

from Custom_apps.dashboard.models import Product as My_product
from Custom_apps.dashboard.tailor.models import *
from Custom_apps.cart.models import Cart
from Custom_apps.dashboard.models import *
from Custom_apps.customer.models import UserProfile
from Custom_apps.customer.forms import UserAddressForm


# Write your views here
class HomeView(CoreHomeView):

	template_name = "index.html"
	result_page = "search.html"

	def get(self, request, **kwargs):

		context = {}
		if request.session['type']:

			if request.session['type'] == "index":

				boutique_obj = Boutique.objects.filter(featured=True, active=True)
				context['featured'] = boutique_obj

			elif request.session['type'] == "boutique":

				return self.Boutique_page()

			elif request.session['type'] == "tailor":

				return self.Tailor_page()

		return render(request, self.template_name, context)

	# Renders boutique page when called using domain name
	def Boutique_page(self, **kwargs):
		
		context = {}
		bout_id = self.request.session['type_id']

		self.request.session['boutique_id'] = bout_id
		bout_obj = Boutique.objects.get(id=bout_id)

		# Favourite Boutique listing
		try:
			user_id = self.request.user.id
			user_obj = User.objects.get(id=user_id)
		except:
			pass
		try:
			fav_obj = Boutique_favourite.objects.get(boutique=bout_obj, user=user_obj)
		except:
			fav_obj = 0

		prod_obj = Product.objects.filter(boutique=bout_obj).order_by('-id')
		feat_obj = Product.objects.filter(boutique=bout_obj, featured=True).order_by('-id')[:2]
		best_obj = Product.objects.filter(boutique=bout_obj).order_by('id')[:2]

		cate_obj = Category.objects.none()
		for i in prod_obj:
			cate_obj = cate_obj | Category.objects.filter(name=i.category.name)

		context['boutique'] = bout_obj
		context['boutique_products'] = prod_obj
		context['featured_products'] = feat_obj
		context['best_products'] = best_obj
		context['fav_boutique'] = fav_obj
		context['category'] = cate_obj

		if bout_obj.template != "":
			self.template_name = bout_obj.template.name
		else:
			self.template_name = "boutique.html"

		return render(self.request, self.template_name, context)

	# Renders tailor page when called using domain name
	def Tailor_page(self, **kwargs):
		
		context = {} 
		self.template_name = "tailors.html"
		tailor_id = self.request.session['type_id']
		tailor_obj = Tailor.objects.get(id=tailor_id)
		tailor_category_obj = TailorCategory.objects.filter(tailor=tailor_obj)
		tailor_portfolio_obj = TailorPortfolio.objects.filter(tailor=tailor_obj)
		self.request.session['tailor_id'] = tailor_id

		context['tailor'] = tailor_obj
		context['tailor_category'] = tailor_category_obj
		context['tailor_portfolio'] = tailor_portfolio_obj


		return render(self.request, self.template_name, context)

	# Search results will be rendered from this method
	def post(self, request):

		context = {}
		keyword = self.request.POST.get('search','')
		if keyword != '':
			pro_obj = Product.objects.filter( Q(description__icontains=keyword) | 
											Q(name__icontains=keyword) | 
											Q(tags__icontains=keyword) | 
											Q(color__name__icontains=keyword) | 
											Q(boutique__name__icontains=keyword) | 
											Q(boutique__location__icontains=keyword) | 
											Q(boutique__description__icontains=keyword) ).values('boutique').distinct()

			all_bs = Boutique.objects.none()

			for i in pro_obj:
				all_bs = all_bs | Boutique.objects.filter(id=i['boutique'])

			length = len(pro_obj)
			if length < 1:
				messages.warning(request, "No matches found, Please try again with another keyword")
			else:
				messages.success(request, str(length) + " results found for  " + str(keyword))
				context['boutique'] = all_bs
		elif keyword == '':
			messages.error(request, "Please Enter anything to search for..")

		return render(request, self.result_page, context)


"""
This view can be used to render boutique page. But because of the domain redirection
Above view is used for rendering index page, boutique page, tailor page which need to be
viewed when called using domain
"""
# class BoutiqueView(TemplateView):

# 	"""template name is defined here to not to get the templateview error"""
# 	template_name = "boutique.html"

# 	def get(self, request, **kwargs):

# 		context = {}

# 		bout_id = kwargs['id']
# 		request.session['boutique_id'] = bout_id
# 		bout_obj = Boutique.objects.get(id=bout_id)

# 		# Favourite Boutique listing
# 		try:
# 			user_id = request.user.id
# 			user_obj = User.objects.get(id=user_id)
# 		except:
# 			pass
# 		try:
# 			fav_obj = Boutique_favourite.objects.get(boutique=bout_obj, user=user_obj)
# 		except:
# 			fav_obj = 0

# 		prod_obj = Product.objects.filter(boutique=bout_obj).order_by('-id')
# 		feat_obj = Product.objects.filter(boutique=bout_obj, featured=True).order_by('-id')[:2]
# 		best_obj = Product.objects.filter(boutique=bout_obj).order_by('id')[:2]

# 		cate_obj = Category.objects.none()
# 		for i in prod_obj:
# 			cate_obj = cate_obj | Category.objects.filter(name=i.category.name)

# 		context['boutique'] = bout_obj
# 		context['boutique_products'] = prod_obj
# 		context['featured_products'] = feat_obj
# 		context['best_products'] = best_obj
# 		context['fav_boutique'] = fav_obj
# 		context['category'] = cate_obj

# 		if bout_obj.template != "":
# 			template_name = bout_obj.template.name
# 		else:
# 			template_name = "boutique.html"

# 		return render(request, template_name, context)

"""
The below method is appropriate for rendering the template perfectly but for dynamically
rendering the templates, this method is not enough, so above method is used. If dynamic loading 
of templates is no neccessary come back to this method
"""
	# def get_context_data(self, **kwargs):

	# 	context = super(BoutiqueView, self).get_context_data(**kwargs)
	# 	bout_id = kwargs['id']
	# 	bout_obj = Boutique.objects.get(id=bout_id)
	# 	prod_obj = Product.objects.filter(boutique=bout_obj)

	# 	context['boutique'] = bout_obj
	# 	context['boutique_products'] = prod_obj
	# 	return context


"""
This view is for searching products according to inputs,
this will be an api for ajax calls
"""
class SearchProducts(View):

	def get(self, request):

		wrapper = []

		bout_id = request.session['boutique_id']
		bout_obj = Boutique.objects.get(id=bout_id)
		keyword = request.GET.get('tag','')
		pro_obj = Product.objects.filter( Q(description__icontains=keyword) | Q(name__icontains=keyword) | Q(color__name__icontains=keyword), boutique=bout_obj)
		if len(pro_obj) > 0:
			for pro in pro_obj:

				dic = {}
				dic['id'] = pro.id
				dic['name'] = pro.name
				dic['slug'] = slugify(pro.name)
				dic['price'] = pro.price
				dic['discount'] = pro.discount
				dic['discount_amount'] = pro.discount_amount
				if pro.image.all():
					dic['image'] = pro.image.all()[0].image.url
				else:
					dic['image'] = "/static/Unknown.jpg"
				dic['category'] = pro.category.name
				dic['stitching'] = pro.stitching
				dic['color'] = pro.color.name
				wrapper.append(dic)
			out = json.dumps(wrapper)
		else:
			out = "No products found"

		return HttpResponse(out)


"""
This view is for filtering products according to inputs,
this will be an api for ajax calls
"""

class FilterProducts(View):

	def get(self, request):

		wrapper = []

		cat_name = request.GET.get('cate','')
		try:
			cat_obj = Category.objects.get(name__iexact=cat_name)
			bout_id = request.session['boutique_id']
			bout_obj =  Boutique.objects.get(id=bout_id)
			prod_obj = Product.objects.filter(boutique=bout_obj, category=cat_obj)
		except:
			cat_obj = None
		if cat_obj is not None:
			for pro in prod_obj:

				dic = {}
				dic['id'] = pro.id
				dic['name'] = pro.name
				dic['slug'] = slugify(pro.name)
				dic['price'] = pro.price
				dic['discount'] = pro.discount
				dic['discount_amount'] = pro.discount_amount
				if pro.image.all():
					dic['image'] = pro.image.all()[0].image.url
				else:
					dic['image'] = "/static/Unknown.jpg"
				dic['category'] = pro.category.name
				dic['stitching'] = pro.stitching
				dic['color'] = pro.color.name
				wrapper.append(dic)
			out = json.dumps(wrapper)
		else:
			out = "No products found"


		return HttpResponse(out)
		

# Add Boutique to Favourite Boutiques
class AddToFav(View):

	def get(self, request):

		user_id = request.user.id
		bout_id = request.session['boutique_id']
		user_obj = User.objects.get(id=user_id)
		bout_obj = Boutique.objects.get(id=bout_id)
		print bout_obj.name
		Boutique_favourite(user=user_obj, boutique=bout_obj).save()

		return HttpResponse("Ok")


# Delete Boutique from Favourite Boutiques
class DelToFav(View):

	def get(self, request):

		user_id = request.user.id
		bout_id = request.session['boutique_id']
		user_obj = User.objects.get(id=user_id)
		bout_obj = Boutique.objects.get(id=bout_id)
		Boutique_favourite.objects.filter(user=user_obj, boutique=bout_obj).delete()

		return HttpResponse("Ok")

"""
This API View is for filtering products according to price.
A price slider is used to select price range
"""
class PriceFiltering(View):

	def get(self, request):

		wrapper = []
		price = request.GET.get('price','')
		price = price.split("-")
		minprice = price[0]
		maxprice = price[1]
		bout_id = request.session['boutique_id']
		bout_obj = Boutique.objects.get(id=bout_id)

		prod_obj = Product.objects.filter(boutique=bout_obj, price__lte = maxprice, price__gte=minprice)
		if len(prod_obj) > 0:
			for pro in prod_obj:

					dic = {}
					dic['id'] = pro.id
					dic['name'] = pro.name
					dic['slug'] = slugify(pro.name)
					dic['price'] = pro.price
					dic['discount'] = pro.discount
					dic['discount_amount'] = pro.discount_amount
					if pro.image.all():
						dic['image'] = pro.image.all()[0].image.url
					else:
						dic['image'] = "/static/Unknown.jpg"
					dic['category'] = pro.category.name
					dic['stitching'] = pro.stitching
					dic['color'] = pro.color.name
					wrapper.append(dic)
			out = json.dumps(wrapper)
		else:
			out = "No products found"

		return HttpResponse(out)


# ----------------------------------------------------Product Page Views--------------------------------------------------

# Write your Views Here

class MyProductDetailView(DetailView):
	"""
	Detailed Product View
	"""

	template_name = 'product1.html'
	model = My_product


	def get(self, request, **kwargs):

		context = {}

		bout_id = self.request.session['type_id']

		obj = self.get_object()

		print obj.boutique.id, bout_id

		bout_id = int(bout_id)
		pbout_id = int(obj.boutique.id)

		if pbout_id != bout_id:
			return HttpResponseRedirect("/err404/")

		discount  = obj.discount
		price = int(obj.price)
		discount_price = price - ((price*discount)/100)

		b_obj = Boutique.objects.get(id=bout_id)

		arrivals = My_product.objects.filter(boutique=b_obj).order_by('-added_date')[:2]
		best = My_product.objects.filter(boutique=b_obj).order_by('added_date')[:2]

		flag = 0
		# all_size = obj.size.all()
		out_of_stock = obj.size.filter(stock__gt=0)

		# Checks whether length of all size queryset is equal to length of out of stock queryset
		if len(out_of_stock) == 0:
			flag = 1

		context['discount_price'] = discount_price
		context['arrivals'] = arrivals
		context['best'] = best
		context['product'] = obj
		context['flag'] = flag

		return render(request, self.template_name, context)


class Error404(TemplateView):
	"""404 Error Page"""

	template_name = '404.html'

		
# ----------------------------------------------Tailor Section-------------------------------------------------

# Renders Tailor listing page
class TailorView(TemplateView):

	template_name = 'stitching.html'

	def get_context_data(self, **kwargs):
		context = super(TailorView, self).get_context_data(**kwargs)
		tailor_obj = Tailor.objects.filter(active=True)
		context['tailor'] = tailor_obj
		return context

	def post(self, request):

		context = {}
		keyword = self.request.POST.get('search','')

		if keyword != '':
			tailor_obj = Tailor.objects.filter( Q(description__icontains=keyword) | 
											Q(name__icontains=keyword) | 
											Q(location__icontains=keyword),
											active=True ).distinct()

			# tailor_category = TailorCategory.objects.filter(name__icontains=keyword)
			# if len(tailor_category) > 0:
			# 	for i in tailor_category:
			# 		tailor_obj = tailor_obj | Tailor.objects.filter(id=i.tailor.id)

			
			context['tailor'] = tailor_obj
			if len(tailor_obj) == 0:

				context['message'] = "No tailor found for " + keyword
			elif len(tailor_obj) == 1:

				context['message'] = str(len(tailor_obj)) + " tailor found for " + keyword
			else:
				context['message'] = str(len(tailor_obj)) + " tailors found for " + keyword

		elif keyword == '':
			context['message'] = "Please Enter anything to search for.."
		
		return render(request, self.template_name, context)


# """
# This view is used for set cookie across domains for SingleSignOn(SSO) Functionallity. This view will be called from
# img tags in html pages to set the cookie for the appropriate Domains. This view will only be called if user is 
# Signed In.
# """
# class SetCookieView(TemplateView):
	
# 	def get(self, request, **kwargs):
		
# 		key = 'sessionids'
# 		value = kwargs['cookieid']
# 		response = redirect("/")
# 		response.set_cookie(key, value)
# 		return response



"""This API view will be used for fetch Faq from database to set it in page footer. This view will be called using ajax"""
class FetchFaq(View):
	"""docstring for FetchFaq"""

	def get(self, request):
		
		faq_obj =  Faq.objects.latest('id')
		out = faq_obj
		return HttpResponse(out)
	


# -----------------------------------------------Stiching center section---------------------------------------------
"""Renders data from cart and provides option to stich or not"""
class StitchingCenterView(TemplateView):
	"""docstring for StichingCenterView"""
	
	template_name = "stitching-center.html"

	def dispatch(self, request, *args, **kwargs):

		if not self.request.user.is_authenticated():
			messages.error(request, "User is not logged in. Please login")
			return redirect("/")
		if 'tailor_id' not in request.session:
			messages.error(request, 'Please select a tailor')
			return redirect(reverse('promotions:TailorView'))
		return super(StitchingCenterView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(StitchingCenterView, self).get_context_data(**kwargs)

		if 'custom' in self.request.session:
			del self.request.session['custom']
		try:
			user_id = self.request.user.id
			user_obj = User.objects.get(id=user_id)
			cart_obj = Cart.objects.filter(user=user_obj)
			cart_prod_obj = Cart.objects.none()
			for cart_item in cart_obj:
				if cart_item.stitching != "Stitched":
					if cart_item.status != "complete" and cart_item.type_of_item != "stitching" or cart_item.type_of_item != "custom":
				# if cart_item.status != "complete" and cart_item.type_of_item != "product":
						cart_prod_obj = cart_prod_obj | Cart.objects.filter(id=cart_item.id)

			# Get category of the selected tailor for i have a material option
			tailor_id = self.request.session['tailor_id']
			tailor_obj = Tailor.objects.get(id=tailor_id)
			category = TailorCategory.objects.filter(tailor=tailor_obj)

			context['cart_data'] = cart_prod_obj
			context['category'] = category
		except ObjectDoesNotExist, e:
			pass

		return context

	def post(self, request):
		
		cart_id = request.POST.get('product','')
		tailor_id = self.request.session['tailor_id']
		cart_obj = Cart.objects.get(id=cart_id)
		product_id = cart_obj.product.id
		prod_obj = Product.objects.get(id=product_id)
		category = prod_obj.category.name


		tailor_obj = Tailor.objects.get(id=tailor_id)
		tailor_category_obj = TailorCategory.objects.filter(tailor=tailor_obj, name__icontains=category)

		# Checks if the user supports the category service of the selected product
		if len(tailor_category_obj) == 0:

			messages.error(request, "Selected tailor doesn't support current service, Please select another Tailor")
			return redirect(reverse('promotions:TailorView'))

		else:
			pass

		return redirect(reverse('promotions:product-stitching', kwargs={'id':cart_id}))


"""This view renders the Patterns and all neccessary details for stitching the product"""
class ProductStitchingView(TemplateView):
	"""docstring for ProductStitchingView"""
	
	template_name = "ProductStitching.html"

	@method_decorator(login_required())
	def dispatch(self, request, *args, **kwargs):

		cart_id = kwargs['id']
		try:
			cart_obj = Cart.objects.get(id=cart_id)
		except ObjectDoesNotExist, e:
			return redirect("/")

		return super(ProductStitchingView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):

		context = super(ProductStitchingView, self).get_context_data(**kwargs)

		cart_id = kwargs['id']
		cart_obj = Cart.objects.get(id=cart_id)

		if cart_obj.product:
			prod_obj = Product.objects.get(id=cart_obj.product.id)
			category = prod_obj.category.name
			context['product_content'] = prod_obj

		else:
			category = cart_obj.name

		tailor_id = self.request.session['tailor_id']
		tailor_obj = Tailor.objects.get(id=tailor_id)
		tailor_category_obj = TailorCategory.objects.get(tailor=tailor_obj, name__icontains=category)
		tailor_subcategory_obj = TailorSubCategory.objects.filter(category=tailor_category_obj)

		# setting id for future use
		self.request.session['category_id'] = tailor_category_obj.id

		outerdic = {}
		for sub in tailor_subcategory_obj:

			outerdic[sub] = TailorPatterns.objects.filter(sub_category=sub)

		context['tailor_patterns'] = outerdic
		
		data = 100/len(tailor_subcategory_obj)
		if data < 15:
			data = 15

		# tailor_patterns_obj = TailorPatterns.objects.filter(sub_category=tailor_subcategory_obj)
		
		context['cart_content'] = cart_obj
		context['tailor_content'] = tailor_obj
		context['tailor_category'] = tailor_category_obj
		context['tailor_subcategory'] = tailor_subcategory_obj
		context['length_subcategory'] = len(tailor_subcategory_obj)
		context['wizard_width'] = data
		# context['tailor_patterns'] = tailor_patterns_obj
		

		return context


"""This view renders the pickupform page for rendering pickup address"""
class PickUpFormView(TemplateView):
	"""docstring for PickUpFormView"""
	
	template_name = "pickupform.html"

	@method_decorator(login_required())
	def dispatch(self, request, *args, **kwargs):
	    return super(PickUpFormView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):

		category = self.request.GET.get('select_product')
		if category is not None:
			print category,"category"
			self.request.session['category_id'] = category
			print self.request.session['category_id'],"category"
		self.request.session['custom'] = 1

		context = super(PickUpFormView, self).get_context_data(**kwargs)
		user_id = self.request.user.id

		try:
			user_obj = User.objects.get(id=user_id)
			user_profile_obj = UserProfile.objects.get(user=user_obj)
			address = user_profile_obj.address.get(pickup = True)
			context['form'] = UserAddressForm(instance=address)
			context['address'] = address
		except ObjectDoesNotExist, e:
			context['form'] = UserAddressForm()
			pass

		return context


	def post(self, request, *args, **kwargs):

		context = {}

		if request.user.is_authenticated():
			profile = UserProfile.objects.get(user = request.user)

			try:
				address = profile.address.get(pickup = True)
				if address is not None:

					ads = UserAddressForm(request.POST, instance=address)
					ads.save()

			except Exception, e:

				all_ads =profile.address.all()
				for i in all_ads:
					i.last_used = False
					i.pickup = False
					i.save()

				ads = UserAddressForm(request.POST)
				new_address = ads.save()
				new_address.pickup = True
				new_address.save()

				profile.address.add(new_address)

			messages.success(request, "Your Address has been saved.")


		return HttpResponseRedirect(reverse('promotions:pick-up-form'))
