from django.conf import settings
from importlib import import_module
from django.utils.http import cookie_date

import time

from Custom_apps.dashboard.models import *
from Custom_apps.dashboard.tailor.models import *


class Crosser(object):

	def isDomain(self, url):
		parts = url.split(".")
		if len(parts) > 2:
			if parts[0] == "www":
				return True
			else:
				return False
		else:
			return True



	def process_request(self, request):

		try:
			# Session id propagation
			session_key = request.COOKIES.get(settings.SESSION_COOKIE_NAME, None)

			if session_key is not None:
				request.cookie_name = session_key

			# URL for creating session objects
			urls_list= []
			bots = Boutique.objects.filter(active=True)
			for bot in bots:
				# if self.isDomain(bot.url):
				urls_list.append(bot.url)

			tailors = Tailor.objects.all()
			for tailor in tailors:
				# if self.isDomain(tailor.url):
				urls_list.append(tailor.url)

			urls_list.append("ritzley.com")

			host = request.get_host()
			if host in urls_list: urls_list.remove(host)


			request.session['urls_list'] = urls_list

			if request.user.is_authenticated():
				request.session['logged_in'] = "1"
			else:
				request.session['logged_in'] = "0"


		except Exception, e:
			print str(e)

		return


	def process_response(self, request, response):

		return response


