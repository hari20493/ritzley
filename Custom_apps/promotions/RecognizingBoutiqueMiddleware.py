import re

from django.utils.text import compress_string
from django.utils.cache import patch_vary_headers

from django import http
from Custom_apps.dashboard.models import *
from Custom_apps.dashboard.tailor.models import *

from django.http import HttpResponseRedirect, HttpResponsePermanentRedirect

from django.utils.text import slugify

class Recognizer(object):

    def process_request(self, request):

        # print "----------------------------------------------------------------------"

        try:
            # get URL
            url = request.META['HTTP_HOST']
            # query = request.META['QUERY_STRING']
            path = request.path

            # print "url=="+url
            # print "query=="+query
            # print "path=="+path

            botiq = Boutique.objects.filter(url = url)
            # print botiq

            tailor = Tailor.objects.filter(url = url)
            # print tailor

            if len(botiq)>0:
                botiq = Boutique.objects.get(url = url)
                # print botiq

                request.session['type'] = 'boutique'
                request.session['type_id'] = botiq.id
            elif len(tailor)>0:
                tailor = Tailor.objects.get(url = url)
                # print tailor

                request.session['type'] = 'tailor'
                request.session['type_id'] = tailor.id
            else:
                request.session['type'] = 'index'
                request.session['type_id'] = 0

            # print request.session['type']
            # print request.session['type_id']

        except Exception, e:
            print "Exception undee!!! == "+str(e)

        # print "----------------------------------------------------------------------"

        return None


    def process_response(self, request, response):

        response['redirected'] = True
        return response